% !TeX encoding = UTF-8
% !TeX spellcheck = russian-aot
% !TeX root = book.tex

% 1.jpg

\chapter{Основные понятия аналитической механики}% 1 (было "Основные определения")
\label{ch:fundamentals}

\section{Связи}% 1.1

Материальная (механическая) система рассматривается как совокупность материальных точек.

Устройства, осуществляющие зависимость между величинами, определяющими положения и скорости точек системы, называются \scterm{связями}.

Наличие связей приводит к выполнению этих зависимостей при движении системы независимо от того, какими являются действующие на систему силы и начальные условия движения системы.

Система, на точки которой наложены связи, называется \scterm{несвободной} системой (все механизмы).
При отсутствии связей система называется \scterm{свободной} (например, солнечная система, стая птиц, воздушная среда).

\iffalse

Таким образом, положение точки $M_i$ определяется в инерциальной декартовой системе координат следующим образом:
\begin{equation*}
\scrv{i} = \scx{i}\p\sciv{} + \scy{i}\p\scjv{} + \scz{i}\p\sckv{}, i = \scint{1}{n},
\end{equation*}
где $n$ "--- количество точек в системе.

Пример (не для записи): точка $M$, закреплённая на конце нити длиной $l$, второй конец которой закреплён в начале координат, подчинена связи
\begin{equation*}
l^2 = \inp{\scx{2} + y^2 + z^2} \geq 0,
\end{equation*}
следовательно, расстояние до точки $M$ от начала координат не превышает $l$.

Связи подобного рода "--- неудерживающие (односторонние) "--- задаются неравенствами.

\fi

Формальная запись связи в общем случае представляет из себя функциональную зависимость от радиусов-векторов (координат) и скоростей точек, а также времени, которая соотносится с нулём (может быть больше, меньше или равна ему), характеризуя тем самым тип ограничений, накладываемых связью.
Примерный вид такой формальной записи представлен ниже:
% 2.jpg
\begin{equation*}
f_\lambda\inp{\scrv{i},\ \scVv{i},\ t} = 0,
\end{equation*}
где
$i = 1\dots n$ "--- количество точек;
$\lambda = 1\dots S$ "--- количество связей;
\scrv{} "--- радиус-вектор точки;
\scVv{} "--- скорость точки;
$t$ "--- время.

Связи можно классифицировать по различным признакам.

Наиболее важное значение имеют порционные или \scterm{голономные} связи.
\scterm{Голономная связь} "--- механическая связь, налагающая ограничения только на положения (или перемещения) точек и тел системы.

Связи, в уравнения которых помимо координат входят скорости, называются \scterm{кинематическими} (\scterm{неголономными}).

Связи называются \scterm{стационарными} (\scterm{склерономными}), если время $t$ не входит в явном виде в уравнение связи.

Связи, в уравнение которых в явном виде входит время $t$, называются \scterm{нестационарными} (\scterm{реономными}).

Связи, уравнения которых записываются равенствами, называются \scterm{двусторонними} (\scterm{удерживающими}).

Связи, уравнения которых записываются неравенствами, называются \scterm{односторонними} (\scterm{неудерживающими}).

В качестве примера голономной связи можно рассмотреть маятник, представляющий собой тело (принимаемое за материальную точку), подвешенное к неподвижной опоре.
В такой системе будут иметься связи, определяемые самой опорой (неподвижность верхней точки подвеса), и связь, определяемая типом подвеса.

Если подвес будет реализован в виде жёсткого стержня, то связь окажет ограничение только на координаты конца маятника (т.\:е. будет голономной); её длина не будет меняться во времени (т.\:е. она будет стационарной), а расстояние от конца стержня до точки подвеса не сможет меняться ни в большую, ни в меньшую сторону (т.\:е. связь будет двусторонней).
Формальная запись связей такого типа будет иметь вид:
\begin{equation*}
f_\lambda\inp{\scrv{i}} = 0,
\end{equation*}
где
$i = 1\dots n$ "--- количество точек;
$\lambda = 1\dots S$ "--- количество связей;
\scrv{i} "--- радиус-вектор точки.

Если подвес реализовать в виде нити, то из перечисленных выше характеристик изменится только одна: расстояние от конца нити до точки подвеса в этом случае не может быть больше, чем длина нити (если принять её нерастяжимой), но может быть меньше этой длины (т.\:е. связь станет односторонней, сохранив остальные характеристики).
Формальная запись связей такого типа будет иметь вид:
\begin{equation*}
f_\lambda\inp{\scrv{i}} \leq 0,
\end{equation*}
где
$i = 1\dots n$ "--- количество точек;
$\lambda = 1\dots S$ "--- количество связей;
\scrv{i} "--- радиус-вектор точки.

Если же пойти ещё дальше и поместить в месте крепления нити протяжное устройство (по типу лебёдки), которое по заданному закону с течением времени будет вытравливать или, наоборот, приотпускать нить, изменяя тем самым длину маятника, то ограничение на координаты конца маятника будет непосредственно зависеть от времени (связь станет нестационарной).
\begin{equation*}
f_\lambda\inp{\scrv{i},\ t} \leq 0,
\end{equation*}
где
$i = 1\dots n$ "--- количество точек;
$\lambda = 1\dots S$ "--- количество связей;
\scrv{i} "--- радиус-вектор точки,
$t$ "--- время.

Перечисленные ситуации проиллюстрированы на~\cref{img:2-1,img:2-2,img:2-3}.

На~\cref{img:2-1} \scmath{OA} "--- жёсткий стержень.
Тогда справедливо $\scmath{OA} = l = \scconst$;
$\scvec{l} = x\p\sciv{} + y\p\scjv{}$.
Отсюда можно получить
$l^2 = x^2 + y^2$,
или, записывая уравнение связи в явном виде,
$x^2 + y^2 - l^2 = 0$.
Полученное уравнение соответствует голономной стационарной удерживающей связи.

На~\cref{img:2-2} \scmath{OA} "--- нитка длиной
$\scmath{OA} = l = \scconst$.
Тогда для координат свободного конца нити справедливо
$x^2 + y^2 \leq l^2$.
Отсюда можно получить уравнение связи в явном виде:
$x^2 + y^2 - l^2 \leq 0$.
Полученное уравнение соответствует голономной стационарной неудерживающей связи.
\begin{figure}[ht]
    \begin{minipage}{0.49\textwidth}
        \newcommand*{\SCImageWidth}{0.6\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/2-1}
        \input{images/pdf/1_1.tikz}
    \end{minipage}%%%
    \hfill
    \begin{minipage}{0.49\textwidth}
        \newcommand*{\SCImageWidth}{0.6\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/2-2}
        \input{images/pdf/1_2.tikz}
    \end{minipage}
    \begin{minipage}[t]{0.49\textwidth}
%        \caption{Голономная стационарная удерживающая связь}
        \caption{}
        \label{img:2-1}
    \end{minipage}%%%
    \hfill
    \begin{minipage}[t]{0.49\textwidth}
%        \caption{Голономная стационарная неудерживающая связь}
        \caption{}
        \label{img:2-2}
    \end{minipage}
\end{figure}

На~\cref{img:2-3} \scmath{OA} "--- нитка с переменной длиной
$\scmath{OA} = l\inp{t} = l_0 + \scV{}\p t$,
$\scV{} = \scconst$.
Тогда для координат свободного конца нити справедливо
$x^2 + y^2 \leq \inp{l_0 + \scV{}\p t}^2$.
Отсюда можно получить уравнение связи в явном виде:
$x^2 + y^2 - \inp{l_0 + \scV{}\p t}^2 \leq 0$. Полученное уравнение соответствует голономной нестационарной неудерживающей связи.

Связи, накладывающие ограничения на скорости точек, можно рассмотреть на примере движения твёрдого тела, катящегося по горизонтальной плоскости без проскальзывания (\cref{img:3-1})~\SCCite{глава~\RNum{28} <<Принцип возможных перемещений и общее уравнение динамики>>, \pno~357, \S137~<<Классификация связей>>}{Targ}.
Для центра масс катящегося колеса будет постоянной вертикальная координата
$\scyCoM{} = \scconst$,
а его горизонтальная скорость будет ограничена произведением угловой скорости на расстояние от центра масс до мгновенного центра скоростей (точки контакта тела и поверхности):
$\scw{}\p\scPC{}$.
Последняя связь называется \scterm{интегрируемой} (т.\:е. она сводится к голономной связи, вносящей ограничение на координаты: горизонтальная координата цента масс связана с углом поворота тела вокруг мгновенного центра скоростей через расстояние от цента масс до мгновенного центра скоростей):
$\scxCoM{} = \int\scw{}\p\scPC{}\scintdiff{t} = \int\scV{\scCoM{}}\scintdiff{t}$.
\begin{figure}[ht]
    \begin{minipage}{0.49\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/2-3}
        \newcommand*{\SCImageWidth}{0.6\textwidth}
        \input{images/pdf/1_3.tikz}
    \end{minipage}%%%
    \hfill
    \begin{minipage}{0.49\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/3-1}
        \input{images/pdf/1_4.tikz}
    \end{minipage}
    \begin{minipage}[t]{0.49\textwidth}
%        \caption{Голономная нестационарная связь}
        \caption{}
        \label{img:2-3}
    \end{minipage}%%%
    \hfill
    \begin{minipage}[t]{0.49\textwidth}
%        \caption{Интегрируемая кинематическая связь}
        \caption{}
        \label{img:3-1}
    \end{minipage}
\end{figure}

Однако если рассмотреть вместо колеса, приведённого на~\cref{img:3-1}, шар, катящийся по поверхности, то условие нулевой скорости в точке контакта шара и поверхности не может быть сведено к ограничениям в координатах точек, так как центр масс шара может двигаться не прямолинейно.\linebreak
В этом случае связь является неголономной: это кинематическая связь, не сводящаяся к геометрической.
Формальная запись связей такого типа будет иметь вид:
\begin{equation*}
f_\lambda\inp{\scrv{i},\ \scrv{n},\ \scVv{i},\ \scVv{n},\ t} = 0,
\end{equation*}
где
$i = 1\dots n$ "--- количество точек;
$\lambda = 1\dots S$ "--- количество связей.


\section{Обобщённые координаты}% 1.2

Обобщёнными координатами называются не зависящие друг от друга параметры, однозначно определяющие положение системы в пространстве.
Обозначаются~\scq{}.

Минимальное количество обобщённых координат для системы, подчинённой голономным связям, называется \scterm{числом степеней свободы системы}.

Определить число степеней свободы можно с использованием следующего выражения:
$k = 3\p n - S$,
где
$n$ "--- количество точек системы;
$S$ "--- количество связей, наложенных на точки системы;
$k$ "--- число степеней свободы:
\begin{gather*}
k = 3 \pn 2 - \inp{\tikzmark{sc:1}3 + \tikzmark{sc:2}1 + \tikzmark{sc:3}1} = 1;\\[5pt]
\text{\small
\centering
Точка~$O$ закреплена\tikz[remember picture,overlay] \draw[->] (-4em,1.4ex)to[out=90,in=-90] ([shift={(0.5ex,-0.5ex)}]pic cs:sc:1);
\quad
$l = \scconst$\tikz[remember picture,overlay] \draw[->] (-2.8em,1.3ex)to[out=90,in=-90] ([shift={(0.5ex,-0.5ex)}]pic cs:sc:2);
\quad
$\scz{A} = 0$\tikz[remember picture,overlay] \draw[->] (-1.2em,1.3ex)to[out=90,in=-90] ([shift={(0.5ex,-0.5ex)}]pic cs:sc:3);}
\end{gather*}
\begin{equation*}
\scq{} = \phi\inp{t}.
\end{equation*}

\SCBeginFigure
 \centering
% \includegraphics[width=0.35\linewidth]{images/scanned/3-2}
 \newcommand*{\SCImageWidth}{0.3\textwidth}
 \input{images/pdf/1_5.tikz}
 \caption{}
 \label{img:3-2}
\SCEndFigure

Для случая, изображённого на~\cref{img:3-2} (стержень с шарнирным закреплением одного из концов), число степеней свободы определяется по приведённой выше формуле.
Механическая система состоит из двух точек и их подвижность ограничена пятью связями: три связи обеспечивают неподвижность шарнира~$O$ (его координата постоянна по каждой из осей), одна связь ограничивает расстояние между точками (оно равно длине стержня), и ещё одна связь ограничивает подвижность свободного края стержня~$A$ плоскостью~\scoxy{} (его координата~\scz{A} постоянна и равна~\num{0}).
Так как все связи в примере голономные, то минимально необходимое число обобщённых координат совпадёт с числом степеней свободы и будет равно единице.
За обобщённую координату в этом случае можно взять угол поворота стержня вокруг оси шарнира~$O$.

Обобщённые координаты могут быть декартовыми и криволинейными.
При составлении уравнения движения часть обобщённых координат может быть линейными, а часть "--- угловыми.
Всё определяется удобством решения конкретной задачи.
В общем случае обобщённые координаты могут зависеть от декартовых координат:
$\scq{j} = \scq{j}\inp{\scx{i}, \scy{i}, \scz{i}, t}$,
где
$j = 1\dots k$, $k$ "--- число степеней свободы;
$i = 1\dots n$ "--- число точек системы.
Тогда верно и обратное: декартовы координаты являются функциями обобщённых координат:
\begin{equation*}
\begin{aligned}
\scx{i} &= \scx{i}\inp{\scq{1}\dots\scq{k}, t};\\
\scy{i} &= \scy{i}\inp{\scq{1}\dots\scq{k}, t};\\
\scz{i} &= \scz{i}\inp{\scq{1}\dots\scq{k}, t},
\end{aligned}
\end{equation*}
где $i = 1\dots n$.
Тогда радиус-вектор может быть записан как
\begin{equation}\label{eq:1:radv}
\scrv{i} =
\scx{i}\p\sciv{} +
\scy{i}\p\scjv{} +
\scz{i}\p\sckv{} =
\scrv{i}\inp{\scq{1}\dots\scq{k}, t}.
\end{equation}

Если все связи, наложенные на систему, являются стационарными, то запись для радиуса-вектора примет вид
\begin{equation*}
\scrv{i} = \scrv{i}\inp{\scq{1}\dots\scq{k}}.
\end{equation*}

\SCBeginFigure
 \centering
% \includegraphics[width=0.3\linewidth]{images/scanned/3-3}
 \input{images/pdf/1_6.tikz}
 \caption{}
 \label{img:3-3}
\SCEndFigure

Рассмотрим в качестве примера стержень, подвешенный как плоский маятник (\cref{img:3-3}).
Расстояние между точками~$O$ и~$A$ будет неизменным и равным модулю радиуса-вектора:
$\scmath{OA} = \scr{} = \scconst$.
В качестве обобщённой координаты выберем угол поворота стержня вокруг оси цилиндрического шарнира~$O$, отсчитываемый от оси~\scy{}:
$\scq{} = \phi$.
Тогда радиус-вектор точки~$A$ и её декартовы координаты могут быть выражены как функции обобщённой координаты через проекции радиуса-вектора на оси~\scx{} и~\scy{}, а обобщённая координата может быть представлена как арктангенс отношения координат точки~$A$:
\begin{gather*}
\scrv{} = x\p\sciv{} + y\p\scjv{} =
\inp{\scr{}\p\sin\phi}\p\sciv{} +
\inp{\scr{}\p\cos\phi}\p\scjv{};\\
\tan\phi = \frac{x}{y} \implies \phi = \arctan\frac{x}{y}.
\end{gather*}

Следует также отдельно отметить, что обобщённые координаты являются функциями времени $\scq{}\inp{t}$.

% 4.jpg
\section{Обобщённые скорости}% 1.3
Производные по времени от обобщённых координат называются \scterm{обобщёнными скоростями}.
Рассмотрим радиус-вектор $i$-й точки, являющийся функцией двух обобщённых координат (для двух степеней свободы) и времени.
Скорость, являющаяся, по определению, полной производной от радиуса-вектора, будет тогда иметь следующий вид:
\begin{equation*}
\begin{aligned}
\scrv{i} &= \scrv{i}\inp{\scq{1}, \scq{2}, t};\\
\scVv{i} &=
\scD{\scrv{i}}{t} =
\scrdv{i} =
\scPD{\scrv{i}}{\scq{1}}\p\scdq{1} +
\scPD{\scrv{i}}{\scq{2}}\p\scdq{2} +
\scPD{\scrv{i}}{t}.
\end{aligned}
\end{equation*}

Обобщая для числа степеней свободы $k$:
\begin{equation*}
\scVv{i} = \Sumjk{\scPD{\scrv{i}}{\scq{j}}\p\scdq{j}} +
\scPD{\scrv{i}}{t}.
\end{equation*}

В том случае, когда точки системы подвержены стационарным связям:
\begin{equation*}
\begin{aligned}
\scrv{i} &= \scrv{i}\inp{\scint{\scq{1}}{\scq{k}}};\\
\scVv{i} &= \Sumjk{\scPD{\scrv{i}}{\scq{j}}\p\scdq{j}}.
\end{aligned}
\end{equation*}

\section{Виртуальные перемещения}% 1.4
\label{ch:virt}
Пусть радиус-вектор имеет вид
\begin{equation*}
\scrv{i} =
\scrv{i}\inp{\scint{\scq{1}}{\scq{k}};\ t},
\end{equation*}
тогда действительное перемещение (как полный дифференциал) будет определяться формулой
\begin{equation*}
\scractual{i} =
\Sumjk{\scPD{\scrv{i}}{\scq{j}}\p\scdiff{\scq{j}}} +
\scPD{\scrv{i}}{t}\p\scdt{}.
\end{equation*}

\scterm{Виртуальное перемещение} "--- мысленное бесконечно малое перемещение, допускаемое наложенными на систему в данный момент связями (т.\:е. виртуальное перемещение определяется при фиксированном времени).
Другое название "--- \scterm{возможное} перемещение.
Обозначается \scbr{i}.

Виртуальные перемещения для стационарных и нестационарных систем:
\begin{equation*}
\scbr{i} = \Sumjk{\scPD{\scrv{i}}{\scq{j}}\p\scbq{j}}.
\end{equation*}

Виртуальное перемещение \scbr{} является изохронной вариацией радиуса-вектора~\scr{}, т.\:е. его полным дифференциалом при фиксированном времени $t = \scconst$:
\begin{equation*}
\scbr{i} =
\scrv{i}
\inp{\scq{1} + \scbq{1},\ \dotsc,\ \scq{n} + \scbq{n},\ t} -
\scrv{i}
\inp{\scq{1},\ \dotsc,\ \scq{n},\ t} =
\Sumjk{\scPD{\scr{i}}{\scq{j}}\p\scbq{j}},
\end{equation*}
где
\scbq{j} "--- виртуальное перемещение обобщённой координаты;
\scbr{i} "--- виртуальное изменение радиуса-вектора.

Виртуальное перемещение определяется только наложенными на систему в данный момент времени связями (связи фиксируются).
Они не осуществляются, поэтому не требуется ни силы, ни времени.

% 5.jpg
Действительное перемещение \scractual{i} определяется наложенными на систему связями, приложенными к точкам системы в данный момент времени силами, требуют времени (выполняются за промежуток времени ${\Delta t}$).

В качестве примера, на основе которого можно сравнить действительные и виртуальные перемещения, рассмотрим закреплённый в виде маятника стержень (\cref{img:5-1} для виртуального и~\cref{img:5-2} для действительного перемещения).
В случае действительного перемещения его нужно <<осуществить>>, т.\:е. приложить силу, которая приведёт к этому перемещению, и потратить на это перемещение промежуток времени.
Тогда точка на конце маятника переместится в строго определённое положение~${\scrv{A}+\scractual{}}$, определяемое наложенной связью:
если связь стационарная (маятник "--- стержень), то она останется на расстоянии~\scr{};
а если связь меняет свою длину, то точка не только переместится за счёт разворота маятника по обобщённой координате~$\phi$, но и за малый отрезок времени сама связь увеличится или уменьшится,
обеспечив таким образом дополнительное приращение вектора~\scractual{} вдоль отрезка~\scmath{OA}.
\begin{figure}[ht]
    \begin{minipage}{0.49\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/5-1}
        \newcommand*{\SCImageWidth}{0.5\textwidth}
        \input{images/pdf/1_7.tikz}
    \end{minipage}%
    \hfill
    \begin{minipage}{0.49\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/5-2}
        \newcommand*{\SCImageWidth}{0.55\textwidth}
        \input{images/pdf/1_8.tikz}
    \end{minipage}
    \begin{minipage}[t]{0.49\textwidth}
        \caption{}
        \label{img:5-1}
    \end{minipage}%
    \hfill
    \begin{minipage}[t]{0.49\textwidth}
        \caption{}
        \label{img:5-2}
    \end{minipage}
\end{figure}

В случае же виртуального перемещения оно <<не осуществляется>>, т.\:е. перемещение нами только воображается (не требуется ни время, ни сила).
Виртуальное перемещение ограничивается только существующей в текущий момент времени связью (т.\:е. связь как бы замораживается во времени, и даже нестационарная связь не меняет своих параметров).
При этом фиксируются возможности точки~$A$ к перемещению вокруг точки~$O$, а их две: поворот стержня либо по часовой, либо против часовой стрелки (и то, и другое допускается связями).
Оба этих перемещения являются виртуальными, или возможными.

Возможное перемещение \scbr{} и действительное перемещение \scractual{} "--- векторы.

\SCBeginFigure
 \centering
% \includegraphics[width=0.35\linewidth]{images/scanned/5-3}
 \newcommand*{\SCImageWidth}{0.3\textwidth}
 \input{images/pdf/1_9.tikz}
 \caption{}
 \label{img:5-3}
\SCEndFigure

Для \emph{стационарных} голономных связей виртуальные перемещения ничем не отличаются от элементарных действительных перемещений в смысле формул, по которым они определяются.

Для нестационарных систем элементарные перемещения не принадлежат к классу виртуальных.

Рассмотрим далее несколько примеров по определению связи между виртуальным изменением радиуса-вектора и виртуальными перемещениями по обобщённым координатам.

Для закреплённого в виде маятника стержня (\cref{img:5-3}), взяв в качестве обобщённой координаты угол его поворота вокруг цилиндрического шарнира в точке~$O$, осуществим изохронную вариацию по приведённой ранее формуле.
В итоге получим:
\begin{equation*}
  \begin{aligned}
  \scmath{OA} &= \scconst = l;\\
  \scrv{} &=
  \inp{l\p\sin\phi}\p\sciv{} +
  \inp{l\p\cos\phi}\p\scjv{};\\
  \scbr{} &= \inb{
  \inp{l\p\cos\phi}\p\sciv{} +
  \inp{-l\p\sin\phi}\p\scjv{}
  }\p\scbph{}.
  \end{aligned}
\end{equation*}

Для двухзвенного маятника, состоящего из двух шарнирно соединённых стержней~\scmath{OA} и~\scmath{AB} (система, имеющая две степени свободы), рассмотрим два варианта задания обобщённых координат.

Первый вариант: $\scq{1} = \phi$ (угол, отсчитываемый вокруг оси~$O$ от нижнего вертикального до текущего положения звена~\scmath{OA}), $\scq{2} = \psi$ (угол, отсчитываемый вокруг оси~$A$ от нижнего вертикального до текущего положения звена~\scmath{AB}) (\cref{img:5-4}):
\begin{equation*}
  j = 1,2;\quad\scmath{OA} = l_1 = \scconst;\quad\scmath{AB} = l_2 = \scconst;
\end{equation*}
\begin{equation*}
  \scq{1} = \phi;\quad \scq{2} = \psi;
\end{equation*}
\begin{equation*}
  \scrv{A} =
  \inp{l_1\p\sin\phi}\p\sciv{} +
  \inp{l_1\p\cos\phi}\p\scjv{};
\end{equation*}
\begin{equation*}
  \scrv{B} =
  \inp{l_1\p\sin\phi +
  l_2\p\sin\psi}\p\sciv{}+%\SCBrEq{+}
  \inp{l_1\p\cos\phi +
  l_2\p\cos\psi}\p\scjv{}.
\end{equation*}
\begin{equation*}
\scbr{A}=
\inb{
  \inp{l\p\cos\phi}\p\sciv{} +
  \inp{-l\p\sin\phi}\p\scjv{}
  }\p\scbph{};
\end{equation*}
\begin{equation*}
\begin{\SCAlign}
\scbr{B} \SCCondAmp{=}
\inb{
\inp{l_1\p\cos\phi}\p\sciv{} +
\inp{-l_1\p\sin\phi}\p\scjv{}
}\p\scbph{}\SCCondBrEqAmp{+}
\inb{
\inp{l_2\p\cos\psi}\p\sciv{} +
\inp{-l_2\p\sin\psi}\p\scjv{}
}\p\scbps{}\SCBrEqCondAmp{=}
\inp{l_1\p\cos\phi\pn\scbph{} +
l_2\p\cos\psi\pn\scbps{}}\p\sciv{}\SCCondBrEqAmp{+}
\inp{-l_1\p\sin\phi\pn\scbph{} -
l_2\p\sin\psi\pn\scbps{}}\p\scjv{}.
\end{\SCAlign}
\end{equation*}

Второй вариант: $\scq{1} = \alpha$ (угол, отсчитываемый вокруг оси~$O$ от нижнего вертикального до текущего положения звена~\scmath{OA}), $\scq{2} = \beta$ (угол, отсчитываемый вокруг оси~$A$ от прямой, содержащей звено~\scmath{OA}, до текущего положения звена~\scmath{AB}) (\cref{img:5-5}):
\begin{equation*}
  \begin{\SCAlign}
  \scr{A} \SCCondAmp{=}
  \inp{l_1\p\sin\alpha}\p\sciv{} +
  \inp{l_1\p\cos\alpha}\p\scjv{};\\
  \scr{B} \SCCondAmp{=}
  \inp{l_1\p\sin\alpha + l_2\p\sin\inp{\alpha + \beta}}\p\sciv{}\SCCondBrEqAmp{+}
  \inp{l_1\p\cos\alpha + l_2\p\cos\inp{\alpha + \beta}}\p\scjv{};\\
  \scq{1} \SCCondAmp{=} \alpha;\quad
  \scq{2} = \beta;\\
  \scbr{B} \SCCondAmp{=}
  \inb{
  \inp{l_1\p\cos\alpha +
  l_2\p\cos\inp{\alpha + \beta}}\p\scbal{}\SCCondBrEqAmp{+}
  l_2\p\cos\inp{\alpha + \beta}\p\scbbe{}}\p\sciv{}\SCBrEqCondAmp{+}
  \inb{
  \inp{-l_1\p\sin\alpha -
  l_2\p\sin\inp{\alpha + \beta}}\p\scbal{}\SCCondBrEqAmp{-}
  l_2\p\sin\inp{\alpha + \beta}\p\scbbe{}}\p\scjv{}.
  \end{\SCAlign}
\end{equation*}

\begin{figure}[ht]
    \begin{minipage}{0.49\textwidth}
        \newcommand*{\SCImageWidth}{0.7\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/5-4}
        \input{images/pdf/1_10.tikz}
    \end{minipage}%
    \hfill
    \begin{minipage}{0.49\textwidth}
        \centering
%        \includegraphics[width=\textwidth]{images/scanned/5-5}
        \newcommand*{\SCImageWidth}{0.7\textwidth}%
        \input{images/pdf/1_11.tikz}
    \end{minipage}
    \begin{minipage}[t]{0.49\textwidth}
        \caption{}
        \label{img:5-4}
    \end{minipage}%
    \hfill
    \begin{minipage}[t]{0.49\textwidth}
        \caption{}
        \label{img:5-5}
    \end{minipage}
\end{figure}

%\FloatBarrier

Изохронная вариация для радиуса-вектора точки~$A$ будет иметь вид, аналогичный примеру с однозвенным маятником.
А вот виртуальные изменения радиуса-вектора точки~$B$, найденные для разных вариантов выбора задания обобщённых координат, будут существенно отличаться.
Различие в сложности получаемых выражений, заметное даже на таком простом примере, говорит о необходимости разумно подходить к выбору обобщённых координат, особенно при описании сложных систем с большим числом степеней свободы: важно не усложнить себе дальнейшие расчёты уже на этом первом шаге анализа механизма.

% ШРВ: Не уверен, стоит ли давать следующую задачку здесь: я её использовал, когда было времени на курс с запасом и его некуда было девать, как задачку, которую студенты сами у доски решают для закрепления материала и вспоминания теормеха.
% ЛДЮ: всегда можно оформить как "задачу со звёздочкой" (опциональную).

% Вставляем только при общей компиляции.
% TODO: нарисовать рисунок и добавить пояснительный текст.
\ifboolexpr{test {\ifnumcomp{\value{edition}}{=}{0}}}{
\input{part1-task}
}{}

\FloatBarrier

%\clearpage

