PSCyr - это набор красивых русских шрифтов для LaTeX.

Установка PSCyr для различных конфигураций:
* [Windows + MiKTeX](Windows.md)
* [Gentoo-пакет](http://packages.gentoo.org/package/dev-tex/pscyr)
* [Скрипт для установки под texlive@ubuntu13.10](https://gist.github.com/tonkonogov/28b19d9e1d18285b9750)
* [Обсуждение поддержки PSCyr](https://github.com/AndreyAkinshin/Russian-Phd-LaTeX-Dissertation-Template/issues/7)

PSCyr включает следующие шрифты:

* Шрифты с засечками:
  * AcademyPSCyr в начертаниях Regular, Bold, Italic
  * AntiquaPSCyr в начертаниях Regular, Bold, Italic, Bold Italic
  * HandbookPSCyr в начертаниях Regular, Bold, Italic
  * CollegePSCyr в начертаниях Regular, Bold, Italic
  * JournalPSCyr в начертаниях Regular, Bold, Italic
  * Lazurski в начертании Regular
  * TimesNewRomanPSMT в начертаниях Regular, Bold, Italic, Bold Italic
* Рубленые шрифты:
  * ArialMT в начертаниях Regular, Bold, Italic, Bold Italic и Black (сверхжирный)
  * TextbookPSCyr в начертаниях Regular, Bold, Italic
  * MagazinePSCyr в начертаниях Regular, Bold, Italic
* Моноширинные шрифты:
  * CourierNewPSMT в начертаниях Regular, Bold, Italic, Bold Italic
  * ERKurierPSCyr в начертаниях Regular, Bold, Italic, Bold Italic
* Декоративные шрифты:
  * CooperPSCyr
  * AdvertisementPSCyr



https://github.com/AndreyAkinshin/Russian-Phd-LaTeX-Dissertation-Template/issues/7#issuecomment-160998925

Всем привет. Я опишу вариант установки PSCyr, который у меня сработал на Ubuntu 15.10. (Справедливости ради надо сказать, что это просто комбинация инструкции http://welinux.ru/post/3200/ и файлов, которые есть в шаблоне https://ru.sharelatex.com/templates/thesis/russian-phd-latex-dissertation-template Я просто всё разжую.

Итак, что нужно сделать. Скачать шаблон, найти в папке PSCyr файл pscyr0.4d.zip и распаковать его содержимое куда угодно. Чтобы не переписывать пути, папка с содержимым должна называться PSCyr, а не pscyr, как в архиве. Затем надо зайти в терминал, перейти к тому каталогу, где лежит папка PSCyr с содержимым, и выполнить команды из вышеупомянутого руководства:
mkdir ./PSCyr/fonts/map ./PSCyr/fonts/enc
cp ./PSCyr/dvips/pscyr/*.map ./PSCyr/fonts/map/
cp ./PSCyr/dvips/pscyr/*.enc ./PSCyr/fonts/enc/
echo "fadr6t AdvertisementPSCyr "T2AEncoding ReEncodeFont" <t2a.enc <adver4.pfb" >> ./PSCyr/fonts/map/pscyr.map
Дальше надо узнать, где у вас локальный каталог texmf. Для этого выполняем
kpsewhich -expand-var='$TEXMFLOCAL'
С вероятностью около единицы (если это убунта) результат будет /usr/local/share/texmf/. И копируем всё туда:
cp -R ./PSCyr/* /usr/local/share/texmf/

Извините, что сообщаю известные и несложные вещи, но вдруг кому-то пригодится.
Ну и подключаем:
texhash
updmap -sys --enable Map=pscyr.map
mktexlsr
У меня после этого всё заработало.
