% !TeX encoding = UTF-8
% !TeX spellcheck = russian-aot
% !TeX root = book.tex

% 89.jpg

\section{Метод кинетостатики. Принцип Даламбера. Основное уравнение динамики}%8.1/4.7

Основное уравнение динамики движущейся материальной точки в соответствии со вторым законом Ньютона можно записать как
\begin{equation*}
\scm{}\p\scav{} = \scFv{} + \scRv{},
\end{equation*}
где
$\scm{} = \scconst$;
\scav{} "--- абсолютное ускорение;
\scFv{} "--- главный вектор активных сил;
\scRv{} "--- главный вектор сил реакций связей.

Это выражение может быть переписано в следующей форме:
\begin{equation*}
\scFv{} + \scRv{} + \scfdv{} = \scvec{0},
\end{equation*}
где
$\scfdv{} = -\scm{}\p\scav{}$ "--- \scterm{даламберова сила инерции} (или просто \scterm{сила инерции}).

Приведённое выражение носит название <<принцип Даламбера>> и гласит: \sctheor{в любой момент времени движения точки геометрическая сумма задаваемых сил, сил реакций связей и сил инерции равна нулю}.

Расчёт силы инерции связан с получением абсолютного ускорения.
Обычно используется его координатное представление или разбиение на переносную, относительную и кориолисову составляющие:
\begin{equation*}
\scfdv{} =
-\scm{}\p\scav{} =
-\scm{}\p\inb*{
\scdderiv{x}\p\sciv{} +
\scddy{}\p\scjv{} +
\scdderiv{z}\p\sckv{}} =
-\scm{}\p\inb*{\scav{\SCTrans} + \scav{\SCRel} + \scav{\scTkor}},
\end{equation*}
где\\
\scav{} "--- абсолютное ускорение;\\
\scav{\SCTrans} "--- переносное ускорение (ускорение той точки связанной системы координат, в которой в данный момент времени находится точка);\\
\scav{\SCRel} "--- относительное ускорение (ускорение точки по отношению к связанной системе координат, которая при этом считается неподвижной);\\
\scav{\scTkor} "--- кориолисово ускорение (мера взаимного влияния переносного и относительного движений).
% ЛДЮ: почему переносное ускорение обозн. буквой e?
% ШРВ: наверное потому что "внешнее" (external)

Переходя от точки к несвободной системе, необходимо осуществить суммирование по всем её точкам.
Для механической системы принцип Даламбера справедлив и для моментов:
\begin{equation*}
\left\{
\begin{aligned}
&\Sumi{\scrv{i}\times\scFv{i}} +
\Sumi{\scrv{i}\times\scRv{i}} +
\Sumi{\scrv{i}\times\scfdv{i}} = 0;\\
&\Sumi{\scFv{i}} +
\Sumi{\scRv{i}} +
\Sumi{\scfdv{i}} = 0.
\end{aligned}
\right.
\end{equation*}

Соответственно, для моментов принцип Даламбера формулируется так: \sctheor{в любой момент времени движения геометрическая сумма моментов активных сил, моментов реакций связей и моментов сил инерции равна нулю}.
Геометрическое суммирование по всем точкам даст вместо каждой суммы главный вектор (сил или моментов сил).

Тогда, в соответствии с уравнениями равновесия геометрической статики:
\begin{equation*}
\left\{
\begin{aligned}
&\scFv{} + \scRv{} = \scvec{0}\\
&\scMOvp{}{\scFv{}} + \scMOvp{}{\scRv{}} = \scvec{0},
\end{aligned}
\right.
\end{equation*}
можно сформулировать имеющие аналогичную форму условия равновесия движущихся систем.
Поскольку они относятся к подвижным объектам, но сформулированы как для неподвижных, они получили названия \scterm{уравнений кинетостатики}.
В любой момент времени геометрическая сумма главных векторов сил равна нулю:
\begin{equation*}
\scFv{} + \scRv{} + \scfdv{} = \scvec{0}.
\end{equation*}

В любой момент времени геометрическая сумма главных моментов активных сил, сил реакции связей и сил инерции равна нулю:
\begin{equation*}
\scMOvp{}{\scFv{}} + \scMOvp{}{\scRv{}} + \scMOvp{}{\scfdv{}} = \scvec{0}.
\end{equation*}


\section{Определение сил инерции и моментов сил инерции для различных случаев движения твёрдого тела}%4.8.1

\subsection{Общий случай движения твёрдого тела}%8.2 -> 4.8

Главный вектор сил инерции может быть записан через сумму сил инерции всех точек тела, а при непрерывном распределении массы по телу "--- через интеграл:
\begin{equation*}
\scfdv{} =
-\Sumi{\scm{i}\p\scav{i}} =
-\scIntm{\scav{}}.
\end{equation*}

Вспомним выражение для центра масс (см.~%
\ifnumequal{\value{edition}}{0}{%
\cref{ch:center-of-mass}%
}{%
%\SCCite{разд.~3.1 (\pno~31)}{UP2023}%
\SCCite{разд.~3.1}{UP2023}%
}):

\begin{equation*}
\scrv{\scCoM{}} =
\frac{\sum\scm{i}\p\scrv{i}}{\sum\scm{i}} =
\frac{\sum\scm{i}\p\scrv{i}}{\scm{}}.
\end{equation*}

Вынося производную за знак суммы (это справедливо, так как масса считается независимой от времени) и применив выражение для центра масс, можно получить:
\begin{equation*}
\scfdv{} =
-\scD*[2]{\Sumi{\scrv{i}\p\scm{i}}}{t} =
-\scD*[2]{\scm{}\p\scrv{\scCoM{}}}{t} =
-\scm{}\p\scav{\scCoM{}}.
\end{equation*}

Таким образом, $\scfdv{} = -\scm{}\p\scav{\scCoM{}}$, где
\scm{} "--- масса тела,
\scav{\scCoM{}} "--- абсолютное ускорение центра масс.

Вспомним теорему об изменении кинетического момента:
\begin{equation*}
\sckmOdv{} = \scMOvp{}{\scFv{}}.
\end{equation*}
Тогда уравнение кинетостатики для моментов сил (при условии введения сил реакций связи в класс активных сил) может быть записано как
\begin{equation*}
\sckmOdv{} + \scMOvp{}{\scfdv{}} = 0.
\end{equation*}

В соответствии с формулой Бура производная от вектора кинетического момента может быть представлена следующим образом:
\begin{equation*}
\sckmOdv{} =
\KeepStyleUnderBrace{\sckmOldv{}}_{\substack{
\text{Относительная}\\
\text{(локальная)}\\
\text{производная}}} +
\KeepStyleUnderBrace{\scwv{}\times\sckmOv{}}_{\substack{
\text{Переносная}\\
\text{(конвективная)}\\
\text{производная}}}.
\end{equation*}

% 91.jpg
Учтём также, что кинетический момент в матричной форме может быть представлен как произведение тензора инерции на вектор-столбец проекций угловой скорости тела:
\begin{equation*}
\scmatn{\sckmO{}} = \scmatn{\scmi{}}\p\scwm{}.
\end{equation*}

С учётом этих выражений получим следующую запись в матричной форме:
\begin{gather*}
\scmatn{\scMp{\scCoM{}}{\scfdv{}}} =
-\inbr*{\scmatn{\scmi{}}\p\scmatn{\epsilon} +
\scwms{}\p\scmatn{\scmi{}}\p\scwm{}};\\
\scmatn{\scfd{}} = -\scm{}\p\scmatn{\sca{\scCoM{}}},
\end{gather*}
где \scmatn{\epsilon} "--- вектор-столбец проекций углового ускорения тела.
Полученное выражение для момента сил инерции должно соответствовать формуле для силы инерции (в качестве полюса должен быть взят центр масс тела, т.\:е. моменты инерции рассчитаны по отношению к центральным осям).


\subsection{Поступательное движение}%8.3.1 -> 4.8.2

В соответствии с условиями поступательного движения тело не меняет своей ориентации, поэтому полученные для общего случая движения формулы приобретают следующий вид:
\begin{gather*}
\scw{} = \epsilon = 0;\\
\scfdv{} = -\scm{}\p\scav{\scCoM{}};\\
\scMvp{\scCoM{}}{\scfdv{}} = 0.
\end{gather*}

\subsection{Вращательное движение вокруг неподвижной оси}%8.3.2 -> 4.8.3
\label{ch:rotat-fixed}

Пусть центр масс тела не лежит на оси вращения \scy{}, как это изображено на~\cref{img:91-1}.

\begin{figure}[ht]
 \centering
% \includegraphics[width=0.4\linewidth]{images/scanned/91-1}
 \input{images/pdf/3-1_20.tikz}
 \caption{}
 \label{img:91-1}
\end{figure}

Вращение по осям \scx{} и \scz{} отсутствует:
\begin{equation*}
\scw{x} = \scw{z} = \scdw{x} = \scdw{z} = 0.
\end{equation*}

Ускорение центра масс тела в этом случае можно представить как суперпозицию его вращательного и осестремительного ускорений:
\begin{gather*}
\scav{\scCoM{}}^{\scTvr} = \scvec{\epsilon}\times\scrv{\scCoM{}};\\
\scav{\scCoM{}}^{\scTos} = \scwv{}\times\inp{\scwv{}\times\scrv{\scCoM{}}};\\
\scav{\scCoM{}}^{\scTvr} \perp \scav{\scCoM{}}^{\scTos};\\
\scav{\scCoM{}} = \scav{\scCoM{}}^{\scTvr} + \scav{\scCoM{}}^{\scTos}.
\end{gather*}
Тогда сила инерции может быть записана как
\begin{equation*}
\scfdv{} =
-\scm{}\p\scav{\scCoM{}} =
-\scm{}\p\inp{\scav{\scCoM{}}^{\scTvr} + \scav{\scCoM{}}^{\scTos}} =
-\scm{}\p\inp{\scvec{\epsilon}\times\scrv{\scCoM{}} + \scwv{}\times\inp{\scwv{}\times\scrv{\scCoM{}}}},
\end{equation*}
или в матричной форме:
\begin{equation*}
\scmatn{\scfd{}} = -\scm{}\p\inbr*{\scskew{\scmatn{\epsilon}}\p\scmatn{\scr{\scCoM{}}} + \scwms{}\p\inp{\scwms{}\p\scmatn{\scr{\scCoM{}}}}};
\end{equation*}
\begin{multline*}
\scmatn{\scfd{}}\SCBrEq{=} -\scm{}\p\inbr*{
\scMatLR{\epsilon_y}\p
\scMatxyz{\scCoM{}} +
\scMatLR{\scw{y}}\p
\inp*{
\scMatLR{\scw{y}}\p
\scMatxyz{\scCoM{}}}}\SCBrEq{=}
-\scm{}\p\inbr*{
\scmatrix{\sczCoM{}\p\epsilon_y \\ 0 \\-\scxCoM{}\p\epsilon_y} +
\scmatrix{-\scw{y}^2\p \scxCoM{} \\ 0 \\ -\scw{y}^2\p \sczCoM{}}}.
\end{multline*}

% 92.jpg

Подставив в выражение для момента сил инерции проекции угловых скоростей и ускорений (нулевые по осям \scx{} и \scz{}), получим:
\begin{multline*}
\scmatn{\scMp{}{\scfdv{}}} =
-\left\{
\scMatSMiXYZ{}\p
\scmatrix{0 \\ \epsilon_y \\ 0} \SCBrEqBrack{+}
\scMatLR{\scw{y}}\p
\inp*{\scMatSMiXYZ{}\p
\scmatrix{0 \\ \scw{y} \\ 0}}\right\}\SCBrEq{=}
-\inbr*{
\scmatrix{
-\scmi{xy}\p\epsilon_y \\
\scmi{y}\p\epsilon_y \\
-\scmi{zy}\p\epsilon_y} +
\scmatrix{
-\scmi{zy}\p\scw{y}^2 \\
0 \\
\scmi{xy}\p\scw{y}^2}}.
\end{multline*}

Если ось вращения "--- главная ось инерции, то выражение для момента сил инерции значительно упростится (за счёт равенства нулю центробежных моментов инерции):
\begin{gather*}
\scmatn{\scfd{}} =
-\scm{}\p\inbr*{
\scmatrix{\sczCoM{}\p\epsilon_y \\ 0 \\ -\scxCoM{}\p\epsilon_y} +
\scmatrix{-\scw{y}^2\p \scxCoM{} \\ 0 \\ -\scw{y}^2\p \sczCoM{}}};
\quad
\scmatn{\scMp{}{\scfdv{}}} = -\scmatrix{0 \\ \scmi{y}\p\epsilon_y \\ 0}.
\end{gather*}

Если ось вращения "--- главная центральная ось инерции, сила инерции окажется нулевой (центр масс лежит на оси вращения):
\begin{gather*}
\scmatn{\scfd{}} = 0;
\quad
\scmatn{\scMp{}{\scfdv{}}} = -\scmatrix{0 \\ \scmi{y}\p\epsilon_y \\ 0}.
\end{gather*}

\subsection{Плоское движение}%8.3.3 -> 4.8.4

Пусть тело, изображённое на~\cref{img:92-1}, движется плоско таким образом, что
\begin{gather*}
\scw{x} = \scw{y} = 0;\quad
\sczCoM{} = 0;\quad
\epsilon_y = \epsilon_x = 0;\quad
\scw{z} \neq 0;\quad
\epsilon_z \neq 0.
\end{gather*}

\begin{figure}[ht]
 \centering
% \includegraphics[width=0.4\linewidth]{images/scanned/92-1}
 \input{images/pdf/3-1_21.tikz}
 \caption{}
 \label{img:92-1}
\end{figure}
Пусть \sczCoM{} "--- главная центральная ось инерции.

Тогда сила инерции может быть представлена в виде:
\begin{gather*}
\scfdv{} = -\scm{}\p\scav{\scCoM{}} =
-\scm{}\p\inp*{\scdderivs{x}{\scCoM{}}\p\sciv{} + \scddy{\scCoM{}}\p\scjv{}};
\quad
\scmatn{\scfd{}} = -\scmatrix{\scm{}\p\scdderivs{x}{\scCoM{}} \\ \scm{}\p\scddy{\scCoM{}} \\ 0}.
\end{gather*}

Подставив условия движения в выражение для момента сил инерции, получим:
\begin{multline*}
\scmatn{\scMp{z}{\scfdv{}}} =
-\left\{
\scmatrix{
\scmi{x} & -\scmi{xy} & 0\\
-\scmi{yx} & \scmi{y} & 0\\
0 & 0 & \scmi{z}}\p
\scmatrix{0 \\ 0 \\ \epsilon_z} \SCBrEqBrack{+}
\scmatrix{
0 & -\scw{z} & 0\\
\scw{z} & 0 & 0\\
0 & 0 & 0}\p
\scmatrix{
\scmi{x} & -\scmi{xy} & 0\\
-\scmi{yx} & \scmi{y} & 0\\
0 & 0 & \scmi{z}}\p
\scmatrix{0 \\ 0 \\ \scw{z}}
\right\} =
-\scmatrix{0 \\ 0 \\ \scmi{z}\p\epsilon_z};
\end{multline*}
\begin{equation*}
\scMp{z}{\scfdv{}} = -\scmi{\sczCoM{}}\p\epsilon_z.
\end{equation*}

% 93.jpg

\subsection{Сферическое движение}%8.3.4 -> 4.8.5

Обычно на практике в качестве осей инерции выбираются главные центральные оси.
Тогда
\begin{equation*}
\scfdv{} = 0;
\end{equation*}
\begin{multline*}
\scmatn{\scMp{}{\scfdv{}}} =
-\left\{
\scmatrix{
\scmi{x} & 0 & 0\\
0 & \scmi{y} & 0\\
0 & 0 & \scmi{z}}\p
\scMatAnyxyz{\epsilon}
\SCBrEqBrack{+}
%+
\scMatS{\scw{}}\p\inp*{
\scmatrix{
\scmi{x} & 0 & 0\\
0 & \scmi{y} & 0\\
0 & 0 & \scmi{z}}
%\SCBrEqBrack{\pn}
\p
\scMatAnyxyz{\scw{}}}
\right\}
\SCBrEq{=}
%=
-\left\{
\scmatrix{
\scmi{x}\p\epsilon_x \\ \scmi{y}\p\epsilon_y \\ \scmi{z}\p\epsilon_z} +
\scmatrix{
\scw{z}\p\scw{y}\p\inp{\scmi{z} - \scmi{y}} \\
\scw{x}\p\scw{z}\p\inp{\scmi{x} - \scmi{z}} \\
\scw{y}\p\scw{x}\p\inp{\scmi{y} - \scmi{x}}}
\right\}.
\ifnumequal{\value{uglyCrutches}}{1}{\hfill}{} % уравнение тут переносится на новую страницу, и Шишкова потребовала чтобы последняя строка была выровняна по левому краю.
\end{multline*}

Если же оси инерции выбраны произвольным образом, то момент сил инерции будет иметь общий вид:
\begin{multline*}
\scmatn{\scMp{}{\scfdv{}}} =
-\left\{
\scMatSMiXYZ{}\p
\scMatAnyxyz{\epsilon} \SCBrEqBrack{+}
\scMatS{\scw{}}\p
\scMatSMiXYZ{}\p
\scMatAnyxyz{\scw{}}\right\}.
\end{multline*}

Если центр масс тела не совпадает с неподвижной точкой, относительно которой происходит вращение (\cref{img:93-1}), то сила инерции окажется ненулевой.
\begin{figure}[ht]
 \begin{minipage}{0.48\textwidth}
  \centering
%  \includegraphics[width=0.8\linewidth]{images/scanned/93-1}
  \input{images/pdf/3-1_22.tikz}
  \caption{}
  \label{img:93-1}
 \end{minipage}
 \hfill
 \begin{minipage}{0.48\textwidth}
  \centering
%  \includegraphics[width=0.8\linewidth]{images/scanned/93-2}
  \input{images/pdf/3-1_23.tikz}
  \caption{}
  \label{img:93-2}
 \end{minipage}
\end{figure}

Следует отметить, что ускорение центра масс в этом случае (так же как и для вращательного движения вокруг неподвижной оси) состоит из осестремительной и вращательной составляющих.
Отличие состоит в том, что при сферическом движении они, в общем случае, не ортогональны друг другу (\cref{img:93-2}):
\begin{gather*}
\scav{\scCoM{}} = \scav{\scCoM{}}^\scTos + \scav{\scCoM{}}^\scTvr =
\scwv{}\times\inp*{\scwv{}\times\scrv{\scCoM{}}} +
\scvec{\epsilon}\times\scrv{\scCoM{}};
\quad
\scav{\scCoM{}}^\scTos \cancel{\perp} \scav{\scCoM{}}^\scTvr.
\end{gather*}

Осестремительное ускорение будет направлено по перпендикуляру~$l_{\scw{}}$ к вектору угловой скорости:
\begin{gather*}
\scav{}^\scTos = \scwv{}\times\scVv{};
\quad
\sca{}^\scTos = \scw{}\p\scV{}\p\sin\scang{\scwv{}}{\scVv{}} =
\scw{}\p\scw{}\p\scr{\scCoM{}}\p\sin\scang{\scwv{}}{\scVv{}} =
\scw{}^2\p l_{\scw{}}.
\end{gather*}
Вращательное ускорение будет направлено ортогонально перпендикуляру~$l_\epsilon$ к вектору углового ускорения:
\begin{gather*}
\scav{}^\scTvr = \scvec{\epsilon}\times\scrv{};
\quad
\sca{}^\scTvr = \epsilon\p\scr{}\p\sin\scang{\scvec{\epsilon}}{\scrv{}} =
\epsilon\p l_\epsilon.
\end{gather*}
Тогда сила инерции будет иметь вид:
\begin{gather*}
\scfdv{} = -\scm{}\p\scav{} \implies \scfdv{} = -\scm{}\p\inp*{\scav{}^\scTos + \scav{}^\scTvr};\\
\scmatn{\scfd{}} = \scmatn{\scfd{}^\scTos} + \scmatn{\scfd{}^\scTvr};\\
\scmatn{\scfd{}} = -\scm{}\p\inp*{\scmatn{\scw{}^2\p l_{\scw{}}} + % NB: три матрицы
\scmatn{\epsilon\p l_\epsilon}}. % BUG: в оригинале было некорректно
\end{gather*}

% 94.jpg


% ШРВ: этот параграф - первый кандидат на отбраковку
\section{Реакции опор при вращении твёрдого тела\\вокруг неподвижной оси}%8.4

Пусть имеется тело, закреплённое на неподвижной оси (\cref{img:94-1}).
Центр масс тела не лежит на оси его вращения.
На вращающееся тело действует сила тяжести (считается приложенной к центру масс).
Требуется определить реакции, возникающие в опоре $A$ (подпятник) \scR{\scx{A}}, \scR{\scy{A}}, \scR{\scz{A}} и в опоре $B$ (подшипник) \scR{\scx{B}}, \scR{\scz{B}} при вращении тела.
Опоры $A$ и $B$ разнесены на расстояние $h$.
\begin{figure}[ht]
 \centering
% \includegraphics[width=0.45\linewidth]{images/scanned/94-1}
 \input{images/pdf/3-1_24.tikz}
 \caption{}
 \label{img:94-1}
\end{figure}

Воспользуемся принципом Даламбера:
\begin{gather*}
% ЛДЮ: а раньше у нолика был вектор
% ШРВ: вроде как одно и то же - нуль-вектор это кажется то же, что и скалярный ноль (т.к. с ним не связано никакое направление в пространстве, ну или связаны разом все)
% ЛДЮ: *заменил на вектор*
\scFv{} + \scRv{} + \scfdv{} = \scvec{0};\\
\scMOvp{}{\scFv{}} +
\scMOvp{}{\scRv{}} +
\scMOvp{}{\scfdv{}} = \scvec{0},
\end{gather*}
где \scFv{} "--- внешняя сила; \scRv{} "--- сила реакции связи.

Представив уравнения в координатной форме и раскрыв силы инерции и моменты сил инерции
%в соответствии с~\cref{ch:rotat-fixed},
(см. с. \pageref{ch:rotat-fixed}),
получим выражения:
\begin{align*}
\text{\scx{}: }&\scF{x} + \scR{\scx{A}} + \scR{\scx{B}} + \scm{}\p \scw{y}^2\p \scxCoM{} - \scm{}\p \sczCoM{}\p\epsilon_y = 0;\\
\text{\scy{}: }&\scF{y} + \scR{\scy{A}} = 0;\\
\text{\scz{}: }&\scF{z} + \scR{\scz{A}} + \scR{\scz{B}} + \scm{}\p\scw{y}^2\p \sczCoM{} + \scm{}\p \scxCoM{}\p\epsilon_y = 0;\\
%\end{align*}
%\begin{align*}
\text{\scx{}: }&\scMp{x}{\scFv{}} + \scR{\scz{B}}\p h + \scmi{xy}\p\epsilon_y + \scmi{zy}\p\scw{y}^2 = 0;\\
\text{\scy{}: }&\scMp{y}{\scFv{}} - \scmi{y}\p\epsilon_y = 0;\\
\text{\scz{}: }&\scMp{z}{\scFv{}} - \scR{\scx{B}}\p h + \scmi{zy}\p\epsilon_y - \scmi{xy}\p\scw{y}^2 = 0.
\end{align*}

В получившихся уравнениях выделенная часть слева представляет собой уравнения статической уравновешенности (если их приравнять нулю):
\begin{samepage}
\begin{align*}
&\tikzmark{sc:8:1l}\scF{x} + \scR{\scx{A}} + \scR{\scx{B}}\tikzmark{sc:8:1r} - \scm{}\p\epsilon_y\p \sczCoM{} + \scm{}\p\scw{y}^2\p \scxCoM{} = 0;\\
&\scF{y} + \scR{\scy{A}}\tikzmark{sc:8:2r} = 0;\\
&\scF{z} + \scR{\scz{A}} + \scR{\scz{B}}\tikzmark{sc:8:3r} + \scm{}\p\scw{y}^2\p \sczCoM{} + \scm{}\p \scxCoM{}\p\epsilon_y = 0;\\
&\scMp{x}{\scFv{}} + \scR{\scz{B}}\p h\tikzmark{sc:8:4r} + \scmi{xy}\p\epsilon_y + \scmi{zy}\p\scw{y}^2 = 0;\\
&\scMp{y}{\scFv{}}\tikzmark{sc:8:5r} - \scmi{y}\p\epsilon_y = 0;\\
&\scMp{z}{\scFv{}} - \scR{\scx{B}}\p h\tikzmark{sc:8:6r} + \scmi{zy}\p\epsilon_y - \scmi{xy}\p\scw{y}^2 = 0.
\end{align*}
\tikz[remember picture,overlay]
\pgfmathsetmacro{\sctxtt}{2.5}
\pgfmathsetmacro{\sctxtb}{-1.3}
\pgfmathsetmacro{\sctxtl}{-0.2}
\pgfmathsetmacro{\sctxtr}{0.2}
\ifnumequal{\value{removemult}}{1}{%
\pgfmathsetmacro{\SCRoundedCorners}{0.5}
}{%
\pgfmathsetmacro{\SCRoundedCorners}{0.2}
}
%\fill[color=blue, rounded corners=0.5em, fill opacity=0.5]
\draw[rounded corners=\SCRoundedCorners em]
([shift={(\sctxtl em,\sctxtb ex)}]pic cs:sc:8:1l) |-
([shift={(\sctxtr em,\sctxtt ex)}]pic cs:sc:8:1r) |-
([shift={(\sctxtr em,\sctxtt ex)}]pic cs:sc:8:2r) |-
([shift={(\sctxtr em,\sctxtt ex)}]pic cs:sc:8:3r) --
([shift={(\sctxtr em,0)}]pic cs:sc:8:3r) --
([shift={(\sctxtr em,\sctxtt ex)}]pic cs:sc:8:4r) |-
([shift={(\sctxtr em,\sctxtt ex)}]pic cs:sc:8:5r) |-
([shift={(\sctxtr em,\sctxtt ex)}]pic cs:sc:8:6r) --
([shift={(\sctxtr em,\sctxtb ex)}]pic cs:sc:8:6r) coordinate (scbrcorn) -|
([shift={(\sctxtl em,\sctxtb - 1 ex)}]pic cs:sc:8:1l) --
cycle;
%\begin{center}
\small Если бы вращение отсутствовало
\tikz[remember picture,overlay] \draw[->] (-0.3em,1.2ex)to[out=45,in=-90] ([shift={(-10pt, -3pt)}]scbrcorn);
%\end{center}
\end{samepage}

Рассмотрим внешние воздействия на анализируемую систему "--- сила тяжести и её момент относительно полюса $A$:
\begin{gather*}
\scFv{} = \scGv{} = -\scm{}\p g\p \scjv{};\\
\scMv{A} = \scrv{\scCoM{}}\times\scGv{};\\
\scmatn{\scM{}} =
\scskew{\scmatn{\scrv{\scCoM{}}}}\p\scmatn{\scG{}} =
\scMatSR{\scCoM{}}\p
\scmatrix{0 \\ -\scm{}\p g \\ 0} =
\scmatrix{\sczCoM{}\p \scm{}\p g \\ 0 \\ -\scxCoM{}\p \scm{}\p g}.
\end{gather*}

% 95.jpg

Подставляя перечисленные воздействия в ранее полученную систему уравнений, получим:
\begin{equation*}
\left\{
\begin{aligned}
&\scR{\scx{A}} + \scR{\scx{B}} - \scm{}\p\epsilon_y\p \sczCoM{} + \scm{}\p\scw{y}^2\p \scxCoM{} = 0;\\
-&\scm{}\p g + \scR{\scy{A}} = 0;\\
&\scR{\scz{A}} + \scR{\scz{B}} + \scm{}\p\scw{y}^2\p \sczCoM{} + \scm{}\p \scxCoM{}\p\epsilon_y = 0;\\
&\scm{}\p g\p \sczCoM{} + \scR{\scz{B}}\p h + \scmi{xy}\p\epsilon_y + \scmi{zy}\p\scw{y}^2 = 0;\\
&\scMp{y}{\scFv{}} = \scmi{y}\p\epsilon_y;\\
-&\scm{}\p g\p \scxCoM{} - \scR{\scx{B}}\p h + \scmi{zy}\p\epsilon_y - \scmi{xy}\p\scw{y}^2 = 0,
\end{aligned}
\right.
\end{equation*}
где $\scMp{y}{\scFv{}}$ "--- внешний активный момент, приводящий систему в движение.

Обычно стараются сделать так, чтобы центр тяжести лежал на оси вращающегося тела (например, гиромотора):
$\scrv{\scCoM{}} = \scyCoM{}\p\scjv{}$.
Тогда ряд слагаемых в приведённых уравнениях окажутся нулевыми:
\begin{gather*}
-\scm{}\p\epsilon_y\p \sczCoM{} + \scm{}\p\scw{y}^2\p \scxCoM{} = 0;\\
\scm{}\p\scw{y}^2\p \sczCoM{} + \scm{}\p \scxCoM{}\p\epsilon_y = 0;\\
\scm{}\p g\p \sczCoM{} = 0;\\
-\scm{}\p g\p \scxCoM{} = 0.
\ifnumequal{\value{uglyCrutches}}{1}{\\\text{\vphantom{LOL}}}{}
\end{gather*}
В этом случае искомые реакции опор будут иметь вид:
\begin{gather*}
\left\{
\begin{aligned}
\scR{\scx{A}} &= -\scR{\scx{B}};\\
\scR{\scy{A}} &= \scm{}\p g;\\
\scR{\scz{A}} &= -\scR{\scz{B}};\\
\scR{\scz{B}} &= -\frac{1}{h}\p\inp*{\scmi{xy}\p\epsilon_y + \scmi{zy}\p\scw{y}^2};\\
\scR{\scx{B}} &=  \frac{1}{h}\p\inp*{\scmi{zy}\p\epsilon_y - \scmi{xy}\p\scw{y}^2}.
\end{aligned}
\right.
\end{gather*}

% 96.jpg

Часто встречается ситуация, когда центр масс не лежит на оси вращения тела, но в качестве осей выбраны главные оси инерции.
В этом случае центробежные моменты инерции будут равны нулю.
Тогда вновь ряд слагаемых в приведённых уравнениях окажутся нулевыми:
\begin{gather*}
\scmi{xy}\p\epsilon_y + \scmi{zy}\p\scw{y}^2 = 0;\\
\scmi{zy}\p\epsilon_y - \scmi{xy}\p\scw{y}^2 = 0.
\end{gather*}

В этом случае искомые реакции опор будут иметь вид:
\begin{equation*}
\left\{
\begin{aligned}
\scR{\scy{A}} &= \scm{}\p g;\\
\scR{\scz{B}} &= \frac{1}{h}\p\inp*{-\scm{}\p g\p \sczCoM{}};\\
\scR{\scx{B}} &= \frac{1}{h}\p\inp*{-\scm{}\p g\p \scxCoM{}};\\
\scR{\scx{A}} &= \scm{}\p \scxCoM{}\p\inp*{\frac{g}{h} - \scw{y}^2} + \scm{}\p \sczCoM{}\p\epsilon_y;\\
\scR{\scz{A}} &= \scm{}\p \sczCoM{}\p\inp*{\frac{g}{h} - \scw{y}^2} - \scm{}\p \scxCoM{}\p\epsilon_y.
\end{aligned}
\right.
\end{equation*}

Если тело вращается вокруг оси с постоянной угловой скоростью, то итоговые выражения для реакций опор ещё несколько упростятся:
\begin{equation*}
\left\{
\begin{aligned}
\scR{\scy{A}} &= \scm{}\p g;\\
\scR{\scz{B}} &= \frac{1}{h}\p\inp*{-\scm{}\p g\p \sczCoM{}};\\
\scR{\scx{B}} &= \frac{1}{h}\p\inp*{-\scm{}\p g\p \scxCoM{}};\\
\scR{\scx{A}} &= \scm{}\p \scxCoM{}\p\inp*{\frac{g}{h} - \scw{y}^2};\\
\scR{\scz{A}} &= \scm{}\p \sczCoM{}\p\inp*{\frac{g}{h} - \scw{y}^2}.
\end{aligned}
\right.
\end{equation*}

\section{Общее уравнение динамики}%8.5 -> 4.10 -> 4.9

Назначение: с использованием общего уравнения динамики могут быть получены дифференциальные уравнения движения механических систем.

В основе%
\ifnumequal{\value{uglyCrutches}}{1}{ }{"---}% по не поддающейся пониманию причине Шишкова сказала убрать здесь тире.
\RNum{2}~закон Ньютона:
$\scm{}\p\scav{} = \scFv{}$.

Общее уравнение динамики ещё называют \scterm{объединённым принципом Даламбера"--~Лагранжа}.

Принцип Даламбера для $i$-й точки имеет вид:
\begin{equation*}
\scFv{i} + \scRv{i} + \scfdv{i} = 0,
\end{equation*}
где
$i = \scint{1}{n}$, $n$ "--- количество точек системы;
\scFv{i} "--- внешние (активные) силы;
\scRv{i} "--- силы реакции связи;
$\scfdv{i} = -\scm{}\p\scav{i} = -\scm{}\p\inp*{\scav{e} + \scav{r} + \scav{\scTkor}}$ "--- даламберовы силы (силы инерции);
\scav{i} "--- абсолютное ускорение точки.

Тогда общее уравнение динамики может быть получено переходом от сил к их виртуальным работам:
\begin{gather*}
\text{\centering \small
Виртуальные работы
\tikz[remember picture,overlay] \draw[->] (-7.8em,-0.7ex)to[out=-90,in=90] ([shift={(0.0em,0.0ex)}]pic cs:sc:8:5:1);
\tikz[remember picture,overlay] \draw[->] (-6.0em,-0.7ex)to[out=-90,in=90] ([shift={(0.0em,0.0ex)}]pic cs:sc:8:5:2);
\tikz[remember picture,overlay] \draw[->] (-4.2em,-0.7ex)to[out=-90,in=90] ([shift={(0.0em,0.0ex)}]pic cs:sc:8:5:3);}\\[8pt]
\overbrace{\Sumin{\scFv{i}\p\scbr{i}}}^{\tikzmark{sc:8:5:1}} +
\overbrace{\Sumin{\scRv{i}\p\scbr{i}}}^{\tikzmark{sc:8:5:2}} +
\overbrace{\Sumin{\scfdv{i}\p\scbr{i}}}^{\tikzmark{sc:8:5:3}} = 0.
\end{gather*}

% 97.jpg

Обычно общее уравнение динамики применяется при идеальных связях.
Тогда (из определения идеальных связей):
\begin{equation*}
\Sumin{\scRv{i}\p\scbr{i}} = 0,
\end{equation*}
и общее уравнение динамики примет вид:
\begin{equation*}
\Sumin{\scFv{i}\p\scbr{i}}+
\Sumin{\scfdv{i}\p\scbr{i}} = 0.
\end{equation*}

С учётом формулы для виртуального перемещения получим выражение для обобщённых сил:
\begin{gather*}
\scbr{i} = \Sumjk{\scpdriqj\p\scbq{j}},\quad\text{$k$ "--- число степеней свободы};\\
\Sumjk{\inp[\Bigg]{
\KeepStyleUnderBrace{
\Sumin{\scFv{i}\p\scpdriqj} +
\Sumin{\scfdv{i}\p\scpdriqj}
}_{\mathclap{\scQ{j} + \scQ{j}^{\scfd{}} = 0}}}
\p\scbq{j}
} = 0.
\end{gather*}

Если связи неидеальны, то с использованием принципа освобождаемости от связей их переводят в задаваемые силы~\scF{i}.
Тогда в случае голономной системы уравнение в обобщённых силах примет тот же вид:
\begin{gather*}
\Sumjk{
\inp[\Bigg]{
\KeepStyleUnderBrace{
\Sumin{\scFv{i}\p\scpdriqj} +
\Sumin{\scRv{i}\p\scpdriqj}
}_{\mathclap{\scQ{j}}}
+
\KeepStyleUnderBrace{
\Sumin{\scfdv{i}\p\scpdriqj}
}_{\mathclap{\scQ{j}^{\scfd{}}}}
}
\p\scbq{j}
} = 0;\\
\scQ{j} + \scQ{j}^{\scfd{}} = 0.
\end{gather*}

Таким образом, объединённый принцип Даламбера"--~Лагранжа может быть сформулирован в следующей форме: \sctheor{в любой момент времени движения механической системы сумма виртуальных работ всех активных сил и сил инерции равна нулю}:
\begin{equation*}
\Sumjk{\inp*{\scQ{j} + \scQ{j}^{\scfd{}}}\p\scbq{j}} = 0.
\end{equation*}

Или, суммируя по точкам системы ($i = \scint{1}{n}$) к которым приложены силы, с последующим разложением в декартовой системе координат:
\begin{gather*}
\Sumin{\inp{\scFv{i} - \scm{i}\p\scddvec{\scr{i}}}\p\scbrnv{i}} = 0;\\
\Sumin{\inp*{
\inp{\scF{ix} - \scm{i}\p\scdderiv{x}_i}\p\scbx{i} +
\inp{\scF{iy} - \scm{i}\p\scddy{i}}\p\scby{i} +
\inp{\scF{iz} - \scm{i}\p\scdderiv{z}_i}\p\scbz{i}
}} = 0.
\end{gather*}

% 98.jpg
\section{Виртуальная работа сил инерции}%8.6 -> 4.11 -> 4.10

\subsection{Поступательное движение}

Тело не меняет своей ориентации, поэтому работа будет определяться только произведением силы на виртуальное перемещение точки её приложения:
\begin{equation*}
\scba{\scfdv{}} =
\scfdv{}\p\scbr{} =
-\scm{}\p\scav{\scCoM{}}\p\scbr{\scCoM{}} =
-\scm{}\p\scav{i}\p\scbr{i}.
\end{equation*}

При поступательном движении для вычисления виртуальной работы сил инерции можно использовать кинематические характеристики любой точки $i$ твёрдого тела.

\subsection{Вращательное движение}

При вращательном движении у тела отсутствуют поступательные степени свободы.
Виртуальная работа силы инерции будет определяться через момент силы инерции (или производную от кинетического момента с учётом знака), умноженный на виртуальное приращение угла поворота:
\begin{equation*}
\scba{\scfdv{}} =
-\scdkm{y}\p\scbph{} =
-\scmi{y}\p\epsilon\p\scbph{},
\end{equation*}
где $\phi$ "--- угол поворота тела вокруг оси вращения;
$\epsilon$ "--- угловое ускорение.

\subsection{Плоское движение}

Пусть движение осуществляется в плоскости \scmath{xz}.
Виртуальная работа будет представлять сумму поступательной и вращательной составляющих.
За полюс принимается центр масс тела:
\begin{equation*}
\scba{\scfdv{}} =
-\scm{}\p\scav{\scCoM{}}\p\scbr{\scCoM{}} - \scmi{\scyCoM{}}\p\epsilon\p\scbph{}.
\end{equation*}

\subsection{Сферическое движение}

При сферическом движении вращение происходит вокруг неподвижной точки, т.\:е. в общем случае есть составляющие по всем трём декартовым осям.
Поэтому формула приобретает матричный вид:
\begin{equation*}
\scba{} = -\scmatn{\scmi{}}\p\scmatn{\epsilon}\p\scbph{},
\end{equation*}
где $\phi$ "--- пространственный угол.

При определении виртуальных работ при сферическом движении обычно используют аппарат вектора конечного поворота~\cite{Lurie,Bran}.

\section{Использование общего уравнения динамики для определения характеристик механической системы}%4.12 -> 4.11

\subsection{Задача <<полиспаст>>}

К изображённой на~\cref{img:99-1} системе блоков подвешены грузы массами $\scm{1} = \SI{10}{\kilogram}$, $\scm{2} = \SI{8}{\kilogram}$.
Механизм находится в поле силы тяжести.
Тела считаются твёрдыми, нити нерастяжимыми, а блоки невесомыми.
Трение в шарнирах отсутствует.
Необходимо определить ускорение груза~\SCTxtNum{2} (\scddy{2}) и силу натяжения нити~\scFT{}.

Введём систему координат (направим ось \scy{} на~\cref{img:99-1} вниз).
Обозначим на рисунке направления ускорений и виртуальных перемещений тел, а также активных сил и сил инерции.
Так как до решения задачи сложно определить направление движения тел в механизме, то выберем его произвольным образом:
предположим, что тело~\SCTxtNum{1}, как имеющее б\'{о}льшую массу, будет двигаться вниз, а тело~\SCTxtNum{2} "--- наверх.
Тогда ускорение и виртуальное перемещение тела~\SCTxtNum{1} будут направлены вниз, а его сила инерции вверх.
Для тела~\SCTxtNum{2} получится наоборот: ускорение и виртуальное перемещение направлены вниз, а сила инерции вверх.
Единственная активная сила в задаче "--- сила тяжести "--- будет направлена вниз для каждого из тел.
Также обозначим силу натяжения нити, действующую от тела~\SCTxtNum{2} вверх (эта сила является силой реакции связи).

\begin{figure}[ht]
 \centering
% \includegraphics[width=0.6\linewidth]{images/scanned/99-1}
 \input{images/pdf/3-1_25.tikz}
 \caption{}
 \label{img:99-1}
\end{figure}

Определение искомой величины ускорения тела~\SCTxtNum{2} осуществим с использованием общего уравнения динамики (принципа \SCUglyUnbreakable{Даламбера"--~Лагранжа}):
\begin{equation*}
\inp{\scGv{1} + \scfdv{1}}\p\scbyv{1} +
\inp{\scGv{2} + \scfdv{2}}\p\scbyv{2} = 0.
\end{equation*}
Следует отметить, что в полученном выражении составляющие, связанные с силой натяжения нити $\scFTv{}\p\scbyv{}$, не учитываются, так как работа идеальных связей равна нулю.

Запишем каждую из присутствующих в общем уравнении динамики~сил:
\begin{gather*}
\scGv{1} = \scm{1}\p\scvec{g};\\
\scGv{2} = \scm{2}\p\scvec{g};\\
\scfdv{1} = -\scm{1}\p\scddyv{1};\\
\scfdv{2} = -\scm{2}\p\scddyv{2}.
\end{gather*}

Тогда в проекции на ось \scy{} (с учётом направления ускорений и виртуальных перемещений) принцип Даламбера"--~Лагранжа примет следующий вид:
\begin{equation*}
\inp{\scm{1}\p g - \scm{1}\p\scddy{1}}\p\scby{1} +
\inp{\scm{2}\p g + \scm{2}\p\scddy{2}}\p\inp{-\scby{1}} = 0.
\end{equation*}

В записанном уравнении присутствуют два виртуальных перемещения, связанных с двумя введёнными обобщёнными координатами (\scy{1} и \scy{2}),
в то время как у механизма присутствует всего одна степень свободы.
Значит, обобщённые координаты, а следовательно и их виртуальные изменения, могут быть выражены друг через друга.

Чтобы получить их соотношение, проанализируем скорости разных точек механизма.
Обозначим за~\scV{} скорость тела~\SCTxtNum{2}.
Оно движется поступательно вместе с той частью нити, что спускается к нему с блока~\SCTxtNum{4}.
Значит, скорость на ободе блока~\SCTxtNum{4} также будет равна~\scV{}.
Следовательно, и скорость той части нити, которая спускается с другой стороны этого блока, также равна~\scV{}.
Тогда скорость крайней правой точки блока~\SCTxtNum{3} "--- также~\scV{}.
Блок~\SCTxtNum{3} не имеет закреплённой оси вращения и совершает плоское движение.
Его мгновенным центром скоростей является точка соприкосновения с вертикальной нитью слева (он как будто катится по меняющей длину опоре).
Тогда скорость центра масс блока~\SCTxtNum{3}, а значит и груза~\SCTxtNum{1}, будет в два раза меньше~\scV{} (соотношение между скоростями точек плоского тела определяется отношением расстояний до мгновенного центра скоростей).
Это же соотношение будет справедливо и для величин ускорений и виртуальных перемещений тел \SCTxtNum{1} и \SCTxtNum{2}:
\begin{equation*}
\scV{\scCoM{1}} = \frac{\scV{}}{2}
\implies \scby{1} = \frac{\scby{2}}{2};\quad
\scddy{1} = \frac{\scddy{2}}{2}.
\end{equation*}

Подставив полученные соотношения кинематических характеристик в общее уравнение динамики, получим:
\begin{gather*} % NB: делим обе части на \scby
\inp*{\scm{1}\p g - \scm{1}\p\frac{\scddy{2}}{2}}\p\inp*{\frac{\cancel{\scby{2}}}{2}} +
\inp*{\scm{2}\p g + \scm{2}\p\scddy{2}}\p\inp*{-\cancel{\scby{2}}} = 0 \SCBrEq{\implies}
\frac{\scm{1}\p g}{2} - \frac{\scm{1}\p\scddy{2}}{4} - \scm{2}\p g - \scm{2}\p\scddy{2} = 0 \SCBrEq{\implies}
\scddy{2} = g\p\frac{\frac{\scm{1}}{2} - \scm{2}}{\frac{\scm{1}}{4} + \scm{2}} \SCBrEq{\implies}
\scddy{2} =
\num{9.81}\p\frac{\frac{\num{10}}{\num{2}} - \num{8}}{\frac{\num{10}}{\num{4}} + \num{8}} =
\num{9.81}\p\frac{\num{-3}}{\num{10.5}} \approx \SI{-2.8}{\metre\per\second\squared}.
\end{gather*}

Рассчитанное значение ускорения тела~\SCTxtNum{2} оказалось отрицательным.
Это означает, что первоначальное предположение о движении этого тела вверх было ошибочным.
Таким образом, тело~\SCTxtNum{2} движется вниз, а тело~\SCTxtNum{1} "--- вверх.

Определение искомой величины силы натяжения нити осуществим с использованием принципа Даламбера:
\begin{equation*}
\scGv{2} + \scfdv{2} + \scFTv{} = 0.
\end{equation*}
Осуществив проецирование на ось \scy{}, получим (здесь уже учитываем реальное направление движения, полученное ранее):
\begin{gather*}
\scm{2}\p g - \scm{2}\p\scddy{2} - \scFTv{} = 0 \SCBrEq{\implies}
\scFTv{} = \scm{2}\p\inp*{g - \scddy{2}} \SCBrEq{\implies}
\scFTv{} = \num{8}\p\inp{\num{9.81} - \num{2.8}} = \SI{56.08}{\newton}.
\end{gather*}

% 100.jpg

\subsection{Задача <<двухступенчатый шкив>>}

Два груза массами~\scm{1} и~\scm{2} подвешены на двух гибких нерастяжимых нитях, которые навёрнуты на барабаны с радиусами~\scrad{1} и~\scrad{2}, имеющие общую ось (\cref{img:100-1}).
Грузы движутся под влиянием силы тяжести.
Нити считаются нерастяжимыми, следовательно, виртуальная работа сил натяжения нитей будет равна нулю.
Трением в механизме пренебрегаем.

\begin{figure}[ht]
 \centering
% \includegraphics[width=0.5\linewidth]{images/scanned/100-1}
 \input{images/pdf/3-1_26.tikz}
 \caption{}
 \label{img:100-1}
\end{figure}

Требуется определить угловое ускорение~$\epsilon$ барабанов, пренебрегая их массами и массой нитей.

Введём систему координат (ось \scy{} направим вниз).
Покажем на рисунке ускорения грузов~\scddyv{1} и~\scddyv{2}, считая что~\scm{2} меньше.
Покажем активные силы~\scGv{1} и~\scGv{2}.
Силы инерции противоположны по направлению ускорениям соответствующих тел.

Выразим возможные перемещения грузов и их координаты через обобщённую координату (угол $\phi$ поворота двухступенчатого шкива):
\begin{gather*}
\scby{1} = \scrad{1}\p\scbph{};\\
\scby{2} = \scrad{2}\p\scbph{};\\
% Это константы интегрирования, но мы не паримся с \scCI, т.к. так нагляднее.
\scy{1} = \scy{1_0} + \scrad{1}\p\phi;\\
\scy{2} = \scy{2_0} + \scrad{2}\p\phi.
\end{gather*}

Тогда ускорения могут быть получены дифференцированием выражений для координат:
\begin{gather*}
\scddy{1} = \scrad{1}\p\scdderiv{\phi} = \scrad{1}\p\epsilon;\\
\scddy{2} = \scrad{2}\p\scdderiv{\phi} = \scrad{2}\p\epsilon.
\end{gather*}

Запишем общее уравнение динамики для анализируемого случая:
\begin{equation*}
\inp{\scG{1} + \scm{1}\p\scddy{1}}\p\inp{-\scby{1}} +
\inp{\scG{2} - \scm{2}\p\scddy{2}}\p\scby{2} = 0.
\end{equation*}

Подставив полученные соотношения для кинематических характеристик, получим:
\begin{gather*}
-\inp{\scm{1}\p g + \scm{1}\p\scrad{1}\p\epsilon}\p\scrad{1}\p\scbph{} +
\inp{\scm{2}\p g - \scm{2}\p\scrad{2}\p\epsilon}\p\scrad{2}\p\scbph{} = 0;\\
-\epsilon\p\inp{\scm{1}\p\scrad{1}^2 + \scm{2}\p\scrad{2}^2} + g\p\inp{\scm{2}\p\scrad{2} - \scm{1}\p\scrad{1}} = 0;\\
\epsilon = g\p\frac{\scm{2}\p\scrad{2} - \scm{1}\p\scrad{1}}{\scm{1}\p\scrad{2}^2 + \scm{2}\p\scrad{2}^2}.
\end{gather*}

% 101.jpg

\subsection{Задача <<кабестан>>}

Вал кабестана (механизма для передвижения грузов) радиуса~\scrad{} приводится в движение постоянным вращающим моментом~\scMv{}, приложенным к рукоятке~\scseg{AB}.
Определить ускорение груза~$C$ массой~\scm{}, если коэффициент трения скольжения о горизонтальную плоскость%
\ifnumequal{\value{uglyCrutches}}{1}{ }{"---}% по не поддающейся пониманию причине Шишкова сказала убрать здесь тире.
\sctrf{}.

Массой каната и кабестана пренебречь.

\begin{figure}[ht]
 \centering
% \includegraphics[width=0.5\linewidth]{images/scanned/101-1}
 \input{images/pdf/3-1_27.tikz} % TODO: растянуть по горизонтали
 \caption{}
 \label{img:101-1}
\end{figure}

Введём систему координат (ось~\scx{} направим влево).
Канат накручивается на вал кабестана; груз двигается влево вдоль оси~\scx{}.
Обозначим угол поворота кабестана как~$\phi$.

Тогда общее уравнение динамики для рассматриваемого случая примет следующий вид:
\begin{equation*}
-\inp*{\sctr{} + \scm{}\p\scdderivs{x}{\scCoM{}}}\p\scbx{\scCoM{}} + \scM{}\p\scbph{} = 0.
\end{equation*}

Для вращательного движения вала кабестана справедливым будет уравнение Эйлера, говорящее о равенстве скорости точки на ободе кабестана произведению угловой скорости его вращения на расстояние от оси до точки (радиус вала).
Аналогичным образом окажутся связаны виртуальные перемещения~\scbx{\scCoM{}} и~\scbph{}:
\begin{equation*}
\scbx{\scCoM{}} = \scrad{}\p\scbph{}.
\end{equation*}

Раскроем силу трения как произведение силы реакции опоры на коэффициент трения:
\begin{equation*}
\sctr{} = \scN{}\p\sctrf{} = \scm{}\p g\p\sctrf{}.
\end{equation*}

Подставив полученные соотношения в общее уравнение динамики, найдём искомое:
\begin{gather*}
-\inp*{\scm{}\p g\p\sctrf{} + \scm{}\p\scdderivs{x}{\scCoM{}}}\p\scrad{}\p\scbph{} + \scM{}\p\scbph{} = 0;\\
\scdderivs{x}{\scCoM{}} = \frac{\scM{} - \scm{}\p g\p\sctrf{}\p\scrad{}}{\scm{}\p\scrad{}}.
\end{gather*}

% 99.jpg

%\clearpage
