% !TeX encoding = UTF-8
% !TeX spellcheck = russian-aot
% !TeX root = book.tex

\subsection{Механическая система с пружиной, демпфером и угловой обобщённой координатой}
\label{ch:1-5-1}

% Вставляем только при раздельной компиляции, т.к. этот рисунок уже есть в part4.
\ifboolexpr{%
test {\ifnumcomp{\value{edition}}{=}{0}}%
}{}%
{%
\begin{figure}[ht]%
 \centering%
% \includegraphics[width=0.6\linewidth]{images/scanned/32-1}
 \input{images/pdf/4_3.tikz}%
 \caption{}%
 \label{img:32-1}%
\end{figure}%
}

Рассмотрим систему, изображённую на~\cref{img:32-1}, состоящую из имеющих массу стержней \scseg{AK} и \scseg{EB}, диска $B$, кольца $K$, ползуна $D$ и находящуюся под действием системы сил:
\scGv{i} (сила тяжести $i$-го звена),
\scFv{\scTpruz} (сила упругости пружины),
\scFv{\scTdemp} (сила вязкого трения в демпфере),
\scFv{} (заданная постоянная сила).
В качестве обобщённой координаты для анализируемой системы логично выбрать угол поворота звена \scseg{AK}, обозначенный $\phi$.
Кинетическая энергия \scT{} и обобщённая сила \scQ{\phi} для такого случая могут быть получены в следующем виде (подробно их расчёт представлен в
\ifnumequal{\value{edition}}{0}{%
\cref{ch:c-coeffs} и \cref{ch:a-coeffs}%
}{%
\SCCite{\ppno~69 и~90}{UP2023}%
}):
\begin{gather*}
\scT{} = \half\p\scAphi{}\p\scderiv{\phi}^2\SCCondCat
\scAphi{} = \scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi;\\
\scQ{\phi} = \scCc{1}\p\sin\phi + \scCc{2}\p\cos\phi + \scCc{3}\p\Sin{2\p\phi} \SCBrEq{+} \scCc{4}\p\sin^2\phi\pn\scderiv{\phi} + \scCc{5}\p\cos^2\phi\pn\scderiv{\phi},
\end{gather*}
где
%\scAc{1}, \scAc{2}, \scAc{3}
\scint{\scAc{1}}{\scAc{3}}
"--- компоненты приведённого момента инерции (константы),
%\scCc{1}, \scCc{2}, \scCc{3}, \scCc{4}, \scCc{5}
\scint{\scCc{1}}{\scCc{5}}
"--- также константы, не зависящие от~$\phi$.

Анализ поведения такой механической системы (с использованием уравнения Лагранжа \RNum{2}~рода) предполагает выполнение следующих действий:
\begin{enumerate}
\item определить положение равновесия системы, находящейся под действием заданной системы сил;
\item определить уравнение движения системы при заданных начальных условиях;
\item определить уравнение малых движений системы около положения равновесия при заданных начальных условиях.
\end{enumerate}

Первое может быть достаточно просто осуществлено с использованием принципа виртуальных перемещений и здесь рассматриваться не будет.
% ШРВ: по идее это рассматривается в невошедшей в 1 часть пособия главе 7 (Определение положения равновесия системы, находящейся под действием заданной системы сил)
% TODO: мы же планируем опубликовывать part7 и part8, вот и вставим ссылку
Будем далее считать, что значение обобщённой координаты \scPst{} в положении устойчивого равновесия системы известно.

Рассмотрим вначале порядок действий для получения уравнения движения такой системы при заданных начальных условиях.
Для этого требуется составить дифференциальное уравнение движения методом Лагранжа, предварительно продифференцировав кинетическую энергию:
\begin{gather*}
\scPD{\scT{}}{\phi} = \half\p\scderiv{\phi}^2\p\inp{
2\p\scAc{2}\p\sin\phi\p\cos\phi - 2\p\scAc{3}\p\sin\phi\p\cos\phi}\SCCondBrEq{=}
\half\p\inp{\scAc{2} - \scAc{3}}\p\Sin{2\p\phi}\p\scderiv{\phi}^2;\\
\scPD{\scT{}}{\scderiv{\phi}} = \scAphi{}\p\scderiv{\phi} =
\inp{\scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi}\p\scderiv{\phi};\\
\scD*{\scPD{\scT{}}{\scderiv{\phi}}}{t} =
\inp{2\p\scAc{2}\p\sin\phi\p\cos\phi\p\scderiv{\phi}
- 2\p\scAc{3}\p\sin\phi\p\cos\phi\p\scderiv{\phi}}\p\scderiv{\phi}\SCBrEq{+}
\inp{\scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi}\p\scdderiv{\phi} =
\inp{\scAc{2} - \scAc{3}}\p\Sin{2\p\phi}\p\scderiv{\phi}^2\SCBrEq{+}
\inp{\scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi}\p\scdderiv{\phi}.
\end{gather*}

\noindent\SCUglyUnbreakable{Тогда можно подставить все составляющие в уравнение Лагранжа \RNum{2}~рода:}
\begin{gather*}
\scD*{\scPD{\scT{}}{\phi}}{t} - \scPD{\scT{}}{\phi} = \scQ{\phi};\\
\inp{\scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi}\p\scdderiv{\phi} +
\half\p\inp{\scAc{2} - \scAc{3}}\p\Sin{2\p\phi}\p\scderiv{\phi}^2 = \scQ{\phi};\\
\scdderiv{\phi}\p\inp{\scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi} +
\half\p\inp{\scAc{2} - \scAc{3}}\p\Sin{2\p\phi}\p\scderiv{\phi}^2\SCBrEq{-}
\scCc{1}\p\sin\phi - \scCc{2}\p\cos\phi - \scCc{3}\p\Sin{2\p\phi} -
\scCc{4}\p\sin^2\phi\pn\scderiv{\phi} - \scCc{5}\p\cos^2\phi\pn\scderiv{\phi} = 0.
\end{gather*}

Интегрирование такого уравнения предполагает его представление в виде системы дифференциальных уравнений, благодаря введению промежуточной переменной \scw{} (скорость изменения обобщённой координаты):
\begin{gather*}
\scdderiv{\phi} = f\inp{\phi, \scderiv{\phi}}\SCCondCat
\scD{\phi}{t} = \scw{}\SCCondCat
\scD{\scw{}}{t} = \scdderiv{\phi} = f\inp{\phi, \scderiv{\phi}}.
\end{gather*}
\begin{equation*}
\left\{
\begin{aligned}
\scD{\phi}{t} &= \scw{};\\
\scD{\scw{}}{t} &= \frac{
\splitfrac{\scCc{1}\p\sin\phi +
\scCc{2}\p\cos\phi +
\scCc{3}\p\Sin{2\p\phi} +
\scCc{4}\p\sin^2\phi\pn\scw{} +{}}
{{}+\scCc{5}\p\cos^2\phi\pn\scw{} - \half\p\inp{\scAc{2} - \scAc{3}}\p\Sin{2\p\phi}\pn\scw{}^2}
}{\scAc{1} + \scAc{2}\p\sin^2\phi + \scAc{3}\p\cos^2\phi}.
\end{aligned}
\right.
\end{equation*}

% 121.jpg (2)

Дифференциальное уравнение 2-го порядка приводится, таким образом, к системе дифференциальных уравнений 1-го порядка:
\begin{equation*}
\left\{
\begin{aligned}
\scD{\phi}{t} &= \scw{};\\
\scD{\scw{}}{t} &= \frac{\scQ{\phi}}{\scAphi{}}.
\end{aligned}
\right.
\end{equation*}

Проблемой является интегрирование второго уравнения системы (из-за наличия в нём переменной $\phi$).

Такого рода дифференциальные уравнения интегрируются численно (как правило, с использованием специального программного обеспечения).
При заданных начальных условиях
$\Eval[\big]{\phi}{t = 0} = \phi_0$,
$\Eval[\big]{\scw{}}{t = 0} = \scw{0}$
эта система интегрируется, и для дальнейшего анализа строятся график зависимости
$\phi = \phi\inp{t}$ и график фазовой траектории, примерный вид которых изображён на~\cref{img:121-1,img:121-2}.
\begin{figure}[ht]
 \begin{minipage}{0.48\textwidth}
  \newcommand*{\SCImageWidth}{0.5\textwidth}
  \centering
  \ifnumequal{\value{bogusImages}}{1}{%
   \input{images/pdf/5_3.tikz}%
   }{% ШРВ сказал тупо взять vynuzd-zatuh и добавить вертикальную подставку
%   \includegraphics[width=0.9\linewidth]{images/scanned/121-1}%
   \input{images/zatuh-1-5-1.tikz}%
  }
  \caption{}
  \label{img:121-1}
 \end{minipage}
 \hfill
 \begin{minipage}{0.48\textwidth}
  \newcommand*{\SCImageWidth}{0.7\textwidth}
  \centering
  \ifnumequal{\value{bogusImages}}{1}{%
  \input{images/pdf/5_6.tikz}%
  }{%
%  \includegraphics[width=0.9\linewidth]{images/scanned/121-2}%
   \input{images/zatuh-1-5-1-spiral.tikz}%
  }
  \caption{}
  \label{img:121-2}
 \end{minipage}
\end{figure}

График $\phi = \phi\inp{t}$, стартуя из начального положения, стремится к значению \scPst{}.
Фазовая траектория начинается из точки
$\inp{\phi_0, \scw{0}}$
и стремится к точке
$\inp{\scPst{}, 0}$, которая определяет состояние равновесия системы.

Характер графиков соответствует одному из трёх случаев свободных затухающих колебаний, подробно рассмотренных в~\cref{ch:svob:zatuh}.

% 3

Рассмотрим порядок действий для определения уравнения малых движений системы около положения равновесия при заданных начальных условиях.
Для этого необходимо составить приближённое дифференциальное уравнение, учитывающее малость движений, т.\:е.:
\begin{equation*}
\phi = \scPst{\scTust} + \scsmall{},
\end{equation*}
где
\scPst{\scTust} соответствует устойчивому положению равновесия,
\scsmall{}, \scderiv{\phi}, \scdderiv{\phi} "--- величины 1-го порядка малости.

С учётом вида функции для кинетической энергии дифференцирование по схеме Лагранжа (без раскрытия \scAphi{}) может быть осуществлено следующим образом:
\begin{gather*}
\scT{} = \half\p\scAphi{}\p\scderiv{\phi}^2\SCCondCat
\scPD{\scT{}}{\phi} = \half\p\scPD{\scAphi{}}{\phi}\p\scderiv{\phi}^2\SCCondCat
\scPD{\scT{}}{\scderiv{\phi}} = \scAphi{}\p\scderiv{\phi};\\
\scD*{\scPD{\scT{}}{\scderiv{\phi}}}{t} = \scPD{\scAphi{}}{\phi}\p\scderiv{\phi}^2 + \scAphi{}\p\scdderiv{\phi}.
\end{gather*}

% 122.jpg (3)

Тогда уравнение Лагранжа \RNum{2}~рода можно представить как
\begin{gather*}
\scAphi{}\p\scdderiv{\phi} + \half\pn\scPD{\scAphi{}}{\phi}\p\scderiv{\phi}^2 - \scQ{\phi} = 0;\\
\tikzmark{sc:idz3:fout}\scTmpFncp{}{\phi, \scderiv{\phi}, \scdderiv{\phi}} =
\scAphi{}\p\scdderiv{\phi} + \half\pn\scPD{\scAphi{}}{\phi}\p\scderiv{\phi}^2 - \scQ{\phi} = 0,
\end{gather*}
где \scTmpFnc{} "--- введённая для удобства функция трёх переменных.
Осуществим её разложение в ряд Тейлора с учётом малости 2-го и более порядков разложения:
\begin{gather*}
\scTmpFncp{}{\phi, \scderiv{\phi}, \scdderiv{\phi}} =
\Eval[\bigg]{\scTmpFnc{}}{\SCEvalPhiSt} +
\Eval{\scPD{\scTmpFnc{}}{\phi}}{
% NB: в оригинале было сокращено до \begin{subarray}{l} \scPst{} \\ 0 \\ 0 \end{subarray}
\SCEvalPhiSt
}\p\Delta\phi +
\Eval{\scPD{\scTmpFnc{}}{\scderiv{\phi}}}{
\SCEvalPhiSt
}\p\Delta\scderiv{\phi} +
\Eval{\scPD{\scTmpFnc{}}{\scdderiv{\phi}}}{
\SCEvalPhiSt
}\p\Delta\scdderiv{\phi}\SCBrEq{+} \inb{\text{малые 2-го и более порядков}}\SCBrEq{=}
\Eval[\bigg]{\scTmpFnc{}}{\SCEvalPhiSt} +
\Eval{\scPD{\scTmpFnc{}}{\phi}}{\SCEvalPhiSt}\p\inp{\phi - \scPst{}} +
\Eval{\scPD{\scTmpFnc{}}{\scderiv{\phi}}}{\SCEvalPhiSt}\p\inp{\scderiv{\phi} - 0} \SCBrEq{+}
\Eval{\scPD{\scTmpFnc{}}{\scdderiv{\phi}}}{\SCEvalPhiSt}\p\inp{\scdderiv{\phi} - 0} =
\Eval[\bigg]{\scTmpFnc{}}{\SCEvalPhiSt} +
\scac{0}\p\inp{\phi - \scPst{}} + \scac{1}\p\scderiv{\phi} + \scac{2}\p\scdderiv{\phi},
\end{gather*}
где для удобства введены следующие обозначения:
\begin{gather*}
\scac{0} =
\Eval{\scPD{\scTmpFnc{}}{\phi}}{\SCEvalPhiSt};\quad
\scac{1} =
\Eval{\scPD{\scTmpFnc{}}{\scderiv{\phi}}}{\SCEvalPhiSt};\quad
\scac{2} =
\Eval{\scPD{\scTmpFnc{}}{\scdderiv{\phi}}}{\SCEvalPhiSt}.
\end{gather*}

Поскольку ранее введённая переменная \scsmall{} соответствует малым колебаниям около положения равновесия, то для малых движений можно осуществить следующее переобозначение переменных:
\begin{gather*}
\scsmall{} = \phi - \scPst{}\SCCondCat
\scsmalld{} = \scderiv{\phi}\SCCondCat
\scsmalldd{} = \scdderiv{\phi}.
\end{gather*}

Из условия равновесия первые два слагаемых функции \scTmpFnc{} будут равны нулю,
а значит её значение в положении равновесия будет определяться исключительно обобщённой силой, которая в соответствии с принципом виртуальных перемещений в положении равновесия также равна нулю:
\begin{gather*}
\tikzmark{sc:idz3:fin}\Eval{\scTmpFnc{}}{\SCEvalPhiSt} =
-\scQp{}{\scPst{}} = 0.
\end{gather*}

Таким образом, разложение функции \scTmpFnc{} в ряд Тейлора до первого порядка, характеризующее с достаточной точностью малые движения механизма, будет иметь вид
\begin{equation}\label{eq:idz3:star}
\scac{0}\p\scsmall{} + \scac{1}\p\scsmalld{} + \scac{2}\p\scsmalldd{} = 0.
\end{equation}

%Два уравнения, связанных стрелочкой, оказались на разных страницах, так что отключаем отрисовку
%\tikz[remember picture,overlay] \draw [->]
%([shift={(-0.1em,0.5ex)}]pic cs:sc:idz3:fout) to
%([shift={(-1.0em,0.5ex)}]pic cs:sc:idz3:fin);

Для определения уравнения малых движений необходимо определить значения коэффициентов \scac{0}, \scac{1}, \scac{2} (констант).
Для этого необходимо взять соответствующие производные от функции \scTmpFnc{} и подставить в них значения переменных, соответствующие положению равновесия системы:
\newcommand*{\SCAbAQPar}{\inp[\Big]{%
\scAphi{}\p\scdderiv{\phi} +%
\half\p\scPD{\scAphi{}}{\phi}\p\scderiv{\phi}^2 - \scQ{\phi}}}
\begin{gather*}
\scac{0} = \Eval{\scPD{}{\phi}\SCAbAQPar}{\SCEvalPhiSt} =
-\Eval{\scPD{\scQ{\phi}}{\phi}}{\SCEvalPhiSt};\\
\scac{1} = \Eval{\scPD{}{\scderiv{\phi}}\SCAbAQPar}{\SCEvalPhiSt}\SCBrEq{=}
\Eval{\inp[\Big]{\scPD{\scAphi{}}{\phi}\p\scderiv{\phi} - \scPD{\scQ{\phi}}{\scderiv{\phi}}}}{\SCEvalPhiSt} =
-\Eval{\scPD{\scQ{\phi}}{\scderiv{\phi}}}{\SCEvalPhiSt};\\
\scac{2} = \Eval{\scPD{}{\scdderiv{\phi}}\SCAbAQPar}{\SCEvalPhiSt}\SCCondBrEq{=}
\Eval[\bigg]{\scAphi{}}{\SCEvalPhiSt}.
\end{gather*}

Уравнение~\eqref{eq:idz3:star} после определения этих коэффициентов может быть переписано в виде
\begin{equation*}
\scsmalldd{} + 2\p\scn{}\p \scsmalld{} + \sck{}^2\p\scsmall{} = 0,
\end{equation*}
где
\scn{} "--- коэффициент затухания,
\sck{} "--- круговая частота собственных незатухающих колебаний;
\begin{gather*}
2\p\scn{} = \frac{\scac{1}}{\scac{2}}\SCCondCat
\sck{}^2 = \frac{\scac{0}}{\scac{2}}.
\end{gather*}

Вид полученного уравнения и его возможные решения $\scsmall{}\inp{t}$ при
$\Eval[\big]{\scsmall{}}{t = 0} = \scsmall{0}$ и
$\Eval[\big]{\scsmalld{}}{t = 0} = \scsmalld{0}$
были подробно изложены в~\cref{ch:svob:zatuh}, посвящённом свободным затухающим колебаниям;
$\scsmall{}\inp{t}$ представляет собой искомый закон малых движений механизма относительно положения равновесия.
