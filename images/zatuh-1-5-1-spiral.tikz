\begin{tikzpicture}

\pgfmathsetmacro\SCPlotOffset{-0.7}
\pgfmathsetmacro\SCPlotMult{-1}
\input{images/zatuh-func.tikz}

\pgfmathsetmacro\SCPlotDiffDelta{1}
\SCDeclareDiff{OscillA}
\SCDeclareDiff{OscillB}
\SCDeclareDiff{OscillC}

\pgfmathparse{\SCPlotMult*(1+\SCPlotOffset)}
\pgfmathsetmacro\SCPlotStartX\pgfmathresult
\pgfmathparse{\SCPlotMult*\SCPlotOffset}
\pgfmathsetmacro\SCPlotEndX\pgfmathresult

% Производная апериодического графика почти не видна, насильно увеличиваем; слабого затухания - уменьшаем
\pgfmathsetmacro\SCPlotACrutch{0.3}
\pgfmathsetmacro\SCPlotCCrutch{2}

\begin{axis}[
no markers,
%xmin=0,
%xmax=105,
%ymin=-\SCPlotA-0.2,
%ymax=\SCPlotA+0.2,
domain=0:100,
samples=\SCPlotResolution,
%samples=500,
axis lines=middle,
%axis y line=left,
%axis x line=bottom,
xlabel=$\phi$,
ylabel=\scw{},
height=5cm, width=\textwidth*0.9,
xtick={\SCPlotStartX, \SCPlotEndX},
xticklabels={$\phi_0$, \scPst{}},
ytick=\empty,
enlargelimits=true,
clip=false,
%axis on top,
%xmajorgrids
%grid=major,
every inner x axis line/.style={AxisLines},
every inner y axis line/.style={AxisLines},
every axis x label/.style={at=(current axis.right of origin),anchor=north},
every axis y label/.style={at=(current axis.above origin),anchor=east}
]

\addplot [PlotLine, red,
attach arrow=0.3
] ({OscillA(x)},{\SCPlotACrutch*OscillAD(x)});

\addplot [PlotLine, green,
attach arrow=0.2
] ({OscillB(x)},{OscillBD(x)});

\addplot [PlotLine, blue,
attach arrow=0.5
] ({OscillC(x)},{\SCPlotCCrutch*OscillCD(x)});

\draw[fill] (axis cs:{OscillA(0)},{\SCPlotACrutch*OscillAD(0)}) circle [radius=1.5pt];
\draw[fill] (axis cs:{OscillB(0)},{OscillBD(0)}) circle [radius=1.5pt];
\draw[fill] (axis cs:{OscillC(0)},{\SCPlotCCrutch*OscillCD(0)}) circle [radius=1.5pt];

\draw[fill] (axis cs:0,0) circle [radius=1.5pt];

\end{axis}

\end{tikzpicture}
